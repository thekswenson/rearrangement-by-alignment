#!/usr/bin/env python3
"""
"""

import os
import sys
import re
import argparse
from math import fabs
from collections import defaultdict

from typing import Hashable, List, Tuple
from typing import Dict
from typing import Set

try:
  import rearrange_by_align
except:
  sys.exit('Try doing "export PYTHONPATH=src:$PYTHONPATH".')

from rearrange_by_align.genome import getGenomes
from rearrange_by_align.genome import parseGenome
from rearrange_by_align.genome import getGenome
from rearrange_by_align.breakpointgraph import BreakpointGraph
from rearrange_by_align.breakpointgraph import ColoredPath
from rearrange_by_align.breakpointgraph import MEDGE
from rearrange_by_align.ilp import getLPstr, lowSlack

try:
  import cplex
except Exception as e:
  print(f'{e}')
  sys.exit('Try doing "export PYTHONPATH=~/software/CPLEX_Studio201/cplex/python/3.8/x86-64_linux:$PYTHONPATH".')
from cplex.exceptions import CplexError


#Switches:
input_chars = False
be_verbose = False
use_threads = False

#Constants:
X = "X"                   #The string for genome X in the variables.
Y = "Y"                   #The string for genome Y in the variables.

PRINT_DEBUG = False


#______________________________________________________________________________#
#|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#
#|:=--=:|  FUNCTIONS  |:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#

def mapEdgeToCycleIndex(cycles: List[ColoredPath], singletons=False
                        ) -> Dict[MEDGE, List[int]]:
  """
  Map multi-edge to indices in cycles that contain it.

  Parameters
  ----------
  cycles : List[ColoredPath]
      the cycles
  singletons : bool
      include cycles that occur in only a single cycle

  Returns
  -------
  Dict[MEDGE, List[int]]
      map from multi-edge to list of indices
  """
  edgeTOcyclei = defaultdict(list)
  for i, c in enumerate(cycles):
    for edge in c.path:
      if edge[0] <= edge[1]:
        edgeTOcyclei[edge].append(i)
      else:
        edgeTOcyclei[(edge[1], edge[0], edge[2])].append(i)

  if not singletons:      #Remove edges that occur in a single cycle
    toremove = []
    for e, ilist in edgeTOcyclei.items():
      if len(ilist) == 1:
        toremove.append(e)

    for e in toremove:
      del edgeTOcyclei[e]

  return edgeTOcyclei


def runLP_MAXECD(cycles: List[ColoredPath],
                 cTOvTOcompatible: Dict[ColoredPath, Dict[str, Set[ColoredPath]]]
                 ) -> List[ColoredPath]:
  """
  Find a maximum Eulerian cycle decomposition using linear programming.

  Parameters
  ----------
  cycles : List[ColoredPath]
      all alternating simple cycles of a graph
  cTOvTOcompatible : Dict[ColoredPath, Dict[str, Set[ColoredPath]]]
      map cycle to vertex (extremity) to compatible cycles

  Returns
  -------
  List[ColoredPath]
      a maximum subset of cycles that cover every edge of the multigraph
      exactly once

  Notes
  -----
      Each edge has at most 1 cycle covering it, so the rows are the edges
      and the columns are the cycles.
  """
  edgeTOcyclei = mapEdgeToCycleIndex(cycles)
  num_cycles = len(cycles)
  num_edges = len(edgeTOcyclei)

  (compatvarsTOcycles, cTOvTOvars,
   num_match_vertices) = getCompatibilityVars(cTOvTOcompatible,
                                              invertList(cycles))
  num_compatible = len(compatvarsTOcycles)

  my_colnames = [f'C_{i}' for i in range(num_cycles)] + list(compatvarsTOcycles)

     #The objective function coefficients:
  obj_coef = [1.0]*num_cycles + [num_edges]*num_compatible   #when maximizing
  #obj_coef = [-1.0]*num_cycles     #when minimizing
     #The upper bound for all the variables (they're all boolean):
  my_ub = [1]*(num_cycles + num_compatible)
  my_ctype = ("B"*(num_cycles + num_compatible))

     #The right-hand side for edge rows will be 1, for compatiblity.
     #We have three types of rows:
     # 1. edge e:        \sum_{c s.t. e \in c} c \leq 1
     # 2. compatibility: for a fixed cycle c and extremity e \in c 
     #                   \sum_{d \in comp(c,e)} p_{c,d} \leq 1
     # 3. link cycle variables to compatibility variables:
     #                   c + d \leq 2p_{c,d} (for every p_{c,d})
  my_rhs = [1.0]*(num_edges + num_match_vertices) + [0]*num_compatible
     #The sense for all the rows. L is for <= and E is for =.
  my_sense = "L"*(num_edges + num_match_vertices) + "G"*num_compatible


       #Now we are ready to setup the CPLEX model:
  try:
    prob = cplex.Cplex()

    #prob.parameters.mip.tolerances.mipgap.set(1.0e-6)
    if use_threads:
      prob.parameters.threads.set(use_threads)

    prob.objective.set_sense(prob.objective.sense.maximize)
    #prob.objective.set_sense(prob.objective.sense.minimize)
    if(not PRINT_DEBUG):
      prob.set_results_stream(None)
      prob.set_warning_stream(None)

      # since lower bounds are all 0.0 (the default), lb is omitted here
    prob.variables.add(obj = obj_coef, ub = my_ub, names = my_colnames,
                       types = my_ctype)

      #Setup the rows (left-hand side of the inequalities):
    rows = []                       #The list of rows
      #  edge inequalities
    for cindices in edgeTOcyclei.values():
      rows.append([[f'C_{i}' for i in cindices], [1]*len(cindices)])

      #  compatibility inequalities
    for vTOvars in cTOvTOvars.values():
      for vars in vTOvars.values():
        rows.append([vars, [1]*len(vars)])

      #  link cycles to compatibility variables
    for var, (i, j) in compatvarsTOcycles.items():
      rows.append([[var, f'C_{i}', f'C_{j}'], [-2, 1, 1]])

    prob.linear_constraints.add(lin_expr = rows, senses = my_sense,
                                rhs = my_rhs)


  except CplexError as exc:
    print("uh oh, CPLEX error:\n\t"+str(exc))
    sys.exit()

  if(be_verbose):
    print("\n"+getLPstr(prob))
    sys.stdout.flush()

  truecyclevars: List[str] = []   #The True cycle vars in solution of the LP.
  truecompatvars: List[str] = []  #The True compatibility vars in the solution.
  try:
    prob.solve()

        # solution.get_status() returns an integer code
    #print("Solution status = " , prob.solution.get_status(), ":", end='')
        # the following line prints the corresponding string
    #print(prob.solution.status[prob.solution.get_status()])
    #print("Solution value  = ", prob.solution.get_objective_value())

      #Sanity check:
    assert(lowSlack(prob))                     #The slack conditions agree.
    assert(prob.solution.get_status() == 101)  #CPLEX says it's optimal.

    truecyclevars = list()
    x = prob.solution.get_values(0, prob.variables.get_num()-1)
    if be_verbose:
      print("Solution:")

    for j in range(prob.variables.get_num()):
      if fabs(x[j]) > 1.0e-10:
        if be_verbose:
          print(prob.variables.get_names(j), end='\n')
          sys.stdout.flush()

        var = prob.variables.get_names(j)
        if isCompatibleVar(var):
          truecompatvars.append(var)
        else:
          truecyclevars.append(prob.variables.get_names(j))

  except CplexError as exc:
    print("uh oh, CPLEX error:\n\t"+str(exc))
    sys.exit()

  for i, c in enumerate(cycles):
    print(f'C_{i}: {c}')
  return [cycles[iFromVar(i)] for i in truecyclevars]

indexre = re.compile(r'C_(\d+)')
def iFromVar(v: str) -> int:
  """
  Return the integer index from the cycles variable name.

  Parameters
  ----------
  v : str
      [description]

  Returns
  -------
  int
      [description]
  """
  if m := indexre.match(v):
    return int(m.group(1))
  else:
    raise(Exception(f'unknown variable format: {v}'))

compatvarre = re.compile(r'C_(\d+)--C_(\d+)')
def isCompatibleVar(var: str) -> bool:
  return bool(compatvarre.match(var))

def getCompatibilityVars(cTOvTOcompatible: Dict[ColoredPath, Dict[str, Set[ColoredPath]]],
                         cycleTOi: Dict[ColoredPath, int]
    ) -> Tuple[Dict[str, ColoredPath], Dict[ColoredPath, Dict[str, List[str]]], int]:
  """
  Return information about the compatability between cycles, describing which
  cycles must be paired in a solution together.

  Parameters
  ----------
  cTOvTOcompatible : Dict[ColoredPath, Dict[str, Set[ColoredPath]]]
      map cycle to vertex (gene extremity) to a set of compatible cycles
  cycleTOi : Dict[ColoredPath, int]
      map a cycle to its index in the list of cycles

  Returns
  -------
  Tuple[Dict[str, ColoredPath], Dict[ColoredPath, Dict[str, List[str]]], int]
      (varsTOcycles, cTOvTOvars, nummatchvertices) where varsTOcycles maps
      compatibility variable names to a pair of compatible cycles, cTOvTOvars
      maps cycle to vertex to compatibility variable name. For each cycle,
      there are a number of vertices that have at least one compatibility
      variable. nummatchvertices is the sum of this number over all cycles.
  """

  cTOvTOvars: Dict[ColoredPath, Dict[str, List[ColoredPath]]] =\
               defaultdict(lambda: defaultdict(list))
  varsTOcycles = {}
  nummatchvertices = 0
  for c, vTOcompatible in cTOvTOcompatible.items():
    for v, compatibles in vTOcompatible.items():
      nummatchvertices += 1
      for compcycle in compatibles:
        indexpair = cycleTOi[c], cycleTOi[compcycle]
        vname = getCompName(*indexpair)
        varsTOcycles[vname] = indexpair
        cTOvTOvars[c][v].append(vname)

  return varsTOcycles, cTOvTOvars, nummatchvertices

def getCompName(c1: int, c2: int) -> str:
  if c2 < c1:
    c1, c2 = c2, c1

  return f'C_{c1}--C_{c2}'

def invertList(l: List[Hashable]) -> Dict[Hashable, int]:
  retval = {}
  for i, val in enumerate(l):
    retval[val] = i

  return retval
#______________________________________________________________________________#
#|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#
#|:=--=:|     Main    |:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#

#Preliminaries: __    __    __    __    __    __    __    __    __    __    __
#__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \_

def main():
  global input_chars, be_verbose, use_threads

  desc = 'Find a maximum cycle decomposition for the given genomes.'
  parser = argparse.ArgumentParser(description=desc)
  inputg = parser.add_argument_group('Input options')
  outputg = parser.add_argument_group('Output options')
  prepg = parser.add_argument_group('Genome preperation')
  modelg = parser.add_argument_group('Evolutionary model')

  parser.add_argument('GENOME', nargs='+',
                      help='input genome filename or string (when -s is used).')
  inputg.add_argument('-s', '--string-input', action='store_true',
                      help='GENOME arguments are strings instead of fasta filenames.')
  inputg.add_argument('-c', '--characters', action='store_true',
                      help='gene names are single characters instead of being "," or ";" or " " delimited.')
  prepg.add_argument('-r', '--rotate', action='store_true',
                     help='rotate the genomes to put the origin at the beginning.')
  outputg.add_argument('-v', '--verbose', action='store_true',
                       help='be verbose.')
  outputg.add_argument('-g', '--breakpoint-graph', metavar='FILENAME',
                       help='make the breakpoint graph PDF.')
  parser.add_argument('-m', '--multiprocessing', metavar='NUM_THREADS',
                      type=int, default=1,
                      help='give CPLEX this many threads to use.')

  args = parser.parse_args()

  genomes = args.GENOME
  input_strings = args.string_input
  input_chars = args.characters
  rotate_genomes = args.rotate
  be_verbose = args.verbose
  use_threads = args.multiprocessing
  bp_graph = args.breakpoint_graph

  file1 = ''
  file2 = ''
  if input_strings:
    if len(genomes) != 2:
      sys.exit(f'Error: expected exactly 2 input strings, got {len(genomes)}.')
  else:
    if input_chars:
      sys.exit('Error: -c can only be used with -s.')
    if len(genomes) > 2:
      sys.exit(f'Error: expected 1 or 2 genome files, got {len(genomes)}.')

    file1 = genomes[0]
    if not os.path.exists(file1):
      sys.exit("Error: file "+file1+" does not exist.\n")

    if len(genomes) == 2:
      file2 = genomes[1]
      if not os.path.exists(file2):
        sys.exit("Error: file "+file2+" does not exist.\n")

#MAIN:    __    __    __    __    __    __    __    __    __    __    __    __
#__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \_


  gx = []   #Genome 1.
  gy = []   #Genome 2.
  if input_strings:
    if input_chars:
      gx = parseGenome(' '.join(genomes[0]))
      gy = parseGenome(' '.join(genomes[1]))
    else:
      gx = parseGenome(genomes[0])
      gy = parseGenome(genomes[1])
  else:     #File input.
    if file2:
      _, gx = getGenome(file1)
      _, gy = getGenome(file2)
    else:
      (_, gx), (_, gy) = getGenomes(file1)

  print(f'{X}: {gx}')
  print(f'{Y}: {gy}')
  sys.stdout.flush()

  bg = BreakpointGraph(gx, gy)
  if bp_graph:
    bg.printGraph(bp_graph)
  
  cycles = bg.getSimpleAlternatingCycles()
  cycleTOvTOcompatible = bg.getCycleCompatibility(cycles)
  decomposition = runLP_MAXECD(cycles, cycleTOvTOcompatible)
  #decomposition = runLP_MAXECD(cycles, bg.getBipartiteGraphsForAssignment(cycles))

  print(f'size: {len(decomposition)}')
  bg.assertDecomposition(decomposition)

if __name__ == "__main__":
  #import doctest  
  #doctest.testmod()
  main()                                           
