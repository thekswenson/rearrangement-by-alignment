#!/usr/bin/env python3
# Krister Swenson                                                  Summer 2011
#
# Simulate a divergence of two sequences.
# The operations are: loss and duplication.
#
# If the evolve flag is given, start with a random sequences (according to
# the given parameters), and then do the number of moves to the ancestor.
# Two lineages are then created to where number of moves are again done.
# The number of moves to the ancestor can be given with -a.
#


import re
import sys
import subprocess
import os
import string
import argparse
from random import choice
from random import randint
from random import random
from random import gauss
from random import seed
from multiprocessing import Pool
from typing import TextIO


  #CONSTANTS:
opmean = 5        #Average length of a loss or duplication.
opvariance = 2    #Variance on the length of a loss or duplication.
GAPCHARS = "-"    #The characters that denote a gap in LP_align output.
DUP = "d"         #The symbol denoting a duplication in the mask.

  #SWITCHES:
do_random = False
be_verbose = False
evolve_moves = 0
ancestral_moves = -1
num_reps = 1
pool_size = 0
detailed_results = False
running_time = False
non_overlapping = False


TMPFILE1 = ".tmp_file_input1.txt"
TMPFILE2 = ".tmp_file_input2.txt"
LPALIGN = "duploss"

rdir = ''


#______________________________________________________________________________#
#|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#
#|:=--=:|  FUNCTIONS  |:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#



def getRandStr(length, asize):
  """
  Create a random string from the given alphabet with the given length and
  given alphabet size.
  """

  alphabet = range(asize)
  return ''.join((str(choice(alphabet))+"," for _ in range(length)))



def getRandSeq(length, asize):
  alphabet = range(asize)
  return [choice(alphabet) for _ in range(length)]



def getRandStr_chars(length, asize):
  """
  Create a random string from the given alphabet with the given length and
  given alphabet size.
  """

  alphabet = string.ascii_letters[:asize]
  if(len(alphabet) < asize):
    sys.exit("The alphabet is too big! We only have the 52 letters.")

  return ''.join((choice(alphabet) for _ in range(length)))





def evolve(seq, num_moves, themoves, movemask):
  """
  Evolve a new string by doing -moves- dups and losses.
  Store the moves in -themoves- and -movemask-.
  """

  s = seq
  for _ in range(num_moves):
    if(randint(0,opmean)):    #If we're doing a loss:
      s = doloss(s, themoves, movemask)
    else:                     #Otherwise, we're doing a duplication:
      s = dodup(s, themoves, movemask)
    assert(len(s) == len(movemask))
    if(not len(s)):
      return s
    if(movemask == [DUP]*len(movemask)):
      sys.exit("Everything is dups: "+str(movemask))

  return s


def doloss(s, moves, mm):
  """
  Return a string with part of it removed.
  """
  global opmean
  global opvariance

  spot = 1
  if(non_overlapping):          #For nonoverlapping entries, 
    spot = choice(getSpots(mm)) #pick a spot not corresponding to a duplication.
  else:                         #Otherwise,
    spot = randint(0,len(s)-1)  #pick any spot.
  moves.append([spot])
  del mm[spot]                  #Remove the entry from the mask.
  return s[:spot]+s[spot+1:]


def getSpots(l):
  """
  Return a list of indices that don't have the DUP character.
  """
  return [i for i in range(len(l)) if l[i] != DUP]



def dodup(s, moves, mm):
  """
  Return a string with part of it duped.
  """
  global opmean
  global opvariance

  start = randint(0,len(s)-1)
  end = start + int(gauss(opmean, opvariance))
  if(end > len(s)):
    end = len(s)

  to = 1
  if(non_overlapping):                    #For nonoverlapping entries, 
    to = choice(getSpots(mm+[0]))         #pick a spot that's not a duplication.
  else:                                   #Otherwise,
    to = randint(0,len(s))                #pick any spot.
  # print "\tdup:"+s[start:]
  # print "\tto:"+str(to)
  # print "\tstartend:"+str(start)+","+str(end)
  moves.append([[start,end],to])
  mm[start:end] = (end-start)*DUP      #Mark the duplication in the mask,
  mm[to:to] = mm[start:end]            # and then copy the duplicated mask.
  #print mm
  return s[:to]+s[start:end]+s[to:]
  



def runAlignFILES(s1, s2, f = False, ops = ""):
  """
  Run LP_align on the given sequences.  If -f- is a valid file descriptor, 
  then write the output of LP_align to the file.
  Pass the options in -ops- to LP_align.
  """
  options = ""
  options += ops
  if be_verbose:
    options += "v"
  if detailed_results:
    options += "d"
  if running_time:
    options += "t"
  if options and options[0] != '-':
    options = '-'+options

                 #Get the random temp filenames.
  ri =  str(random())
  tmpfile1 = TMPFILE1+ri
  tmpfile2 = TMPFILE2+ri
                 #Write the sequences to the files:
  with open(tmpfile1, 'w') as t:
    t.write(s1)

  with open(tmpfile2, 'w') as t:
    t.write(s2)

  command = [rdir+"/"+LPALIGN]
  if options:
    command.append(options)
  command += [tmpfile1, tmpfile2]
  output = str(subprocess.run(command, capture_output=True, text=True).stdout)
  if(f):
    f.write(output)

  os.remove(tmpfile1)
  os.remove(tmpfile2)

  return output


def runAlign(s1: str, s2: str, outfile: TextIO=None, ops=""):
  """
  Run LPALIGN on the given strings.  If `outfile` is a valid file descriptor, 
  then write the output of LPALIGN to the file.
  Pass the options in -ops- to LPALIGN.
  """
  options = "-s"
  options += ops
  if(be_verbose):
    options += "v"
  if(detailed_results):
    options += "d"
  if(running_time):
    options += "t"

  output = subprocess.check_output([rdir+"/"+LPALIGN, options, s1, s2],
                                   stderr=subprocess.STDOUT, text=True)
  if(outfile):
    outfile.write(output)

  return output



def runRandom(args):
  """
  Create two random permutations 
  """
  l, asize, n = args

  filename = "random."+str(l)+"."+str(asize)+"-"+str(n)+".txt"
  with open(filename, 'w') as f:
    s1 = getRandStr(l, asize)
    s2 = getRandStr(l, asize)
    f.write("comparing:\n\t"+s1)
    f.write("\n\t"+s2+"\n")
    f.flush()
    runAlignFILES(s1,s2, f)

  return True
  



def runEvolved(args):
  """
  Start with a random sequence with the given length and alphabet.  Evolve
  the "ancestor".  Then evolve the two child sequences from the ancestor.
  """
  l, asize, n = args
  suffix = str(ancestral_moves)+"."+str(evolve_moves)+"."+str(l)+"."+\
           str(asize)+"-"+str(n)+".txt"

  with open("evolved."+suffix, 'w') as f:
    start = getRandSeq(l, asize)

    moves1 = []
    mask1 = [0]*l
    trueanc = evolve(start, ancestral_moves, moves1, mask1)
    if(not len(trueanc)):
      raise(Exception("The true ancestor is empty."))

    mask2 = [0]*len(trueanc)
    mask3 = [0]*len(trueanc)
    moves2 = []
    moves3 = []
    f.write("true ancestor:"+seq2str(trueanc)+"\n")

    s1 = evolve(trueanc, evolve_moves, moves2, mask2)
    if(not len(s1)):
      print(s1)
      raise(Exception("S1 is empty."))

    s2 = evolve(trueanc, evolve_moves, moves3, mask3)
    if(not len(s2)):
      print(s2)
      raise(Exception("S2 is empty."))

    f.write("comparing:\n\t"+seq2str(s1))
    f.write("\n\t"+seq2str(s2)+"\n")
    f.flush()
    output = runAlignFILES(seq2str(s1,","), seq2str(s2,","), f)

  with open("ancestor."+suffix, 'w') as f:
    m = re.search(r"Ancestor: (.+)", output)
    companc = m.group(1).replace(GAPCHARS, "")
    companc = companc.replace(" , ",",")
  
    f.write("aligning...\n\t"+seq2str(trueanc,",")+"\n\t"+companc+"\n\n")
    f.flush()

    output = runAlignFILES(seq2str(trueanc,","), companc, ops="d")
    m = re.search(r"Cost = (\d+).+Genome X: (.+)\nGenome Y: (.+)", output, re.DOTALL)
    f.write("distance: "+m.group(1)+"\n"+m.group(2)+"\n"+m.group(3))
  

  
def seq2str(s, delim=" "):
  """
  Return a string for the given sequence.
  """
  return delim.join(str(e) for e in s)



#______________________________________________________________________________#
#|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#
#|:=--=:|     Main    |:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#


#Preliminaries: __    __    __    __    __    __    __    __    __    __    __
#__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \_

def main():
  global do_random, be_verbose, evolve_moves
  global ancestral_moves, num_reps, pool_size, detailed_results, running_time
  global non_overlapping, rdir

  desc = 'Simulate a divergence of two sequences under loss and duplication.'
  parser = argparse.ArgumentParser(description=desc)

  parser.add_argument('LENGTH', type=int,
                      help='The length of the genomes.')
  parser.add_argument('ALPHABET_SIZE', type=int,
                      help='The size of the alphabet.')
  parser.add_argument('-e', '--evolve', metavar='NUM_MOVES', type=int,
                      help='evolve two strings by applying NUM_MOVES.')
  parser.add_argument('-a', '--ancestral', metavar='NUM_MOVES', type=int,
                      help='do NUM_MOVES from random string to ancestor (use with -e).')
  parser.add_argument('-n', '--non-overlapping', action='store_true',
                      help='allow only non-overlapping operations (with -e).')
  parser.add_argument('-r', '--random', action='store_true',
                      help='compare random strings.')
  parser.add_argument('-i', '--iterate', metavar='NUM_REPS', type=int, default=1,
                      help='iterate the given number of reps.')
  parser.add_argument('-v', '--verbose', action='store_true',
                      help='be verbose.')
  parser.add_argument('-d', '--details', action='store_true',
                      help='show detailed results.')
  parser.add_argument('-t', '--time', action='store_true',
                      help='show running time.')
  parser.add_argument('-m', '--multiprocessing', metavar='NUM_THREADS', type=int,
                      help='run on multiple processors.')

  args = parser.parse_args()

  length = args.LENGTH
  alphsize = args.ALPHABET_SIZE
  do_random = args.random
  be_verbose = args.verbose
  evolve_moves = args.evolve
  ancestral_moves = args.ancestral
  num_reps = args.iterate
  pool_size = args.multiprocessing
  running_time = args.time
  detailed_results = args.details
  non_overlapping = args.non_overlapping

  if(not ancestral_moves):
    ancestral_moves = evolve_moves


    #Get the directory we're dealing with:
  rdir = os.path.dirname(os.path.realpath(__file__))


  #MAIN:    __    __    __    __    __    __    __    __    __    __    __    __
  #__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \_


  seed()

  result = list()
  if(do_random):
    arglist = zip(num_reps*[length], num_reps*[alphsize], range(num_reps))
    if(pool_size):
      pool = Pool(processes=int(pool_size))       #start the worker processes,
      r = pool.map_async(runRandom, arglist, callback=result.append)
      r.wait()                                    #and crank on them.
    else:
      result = map(runRandom, arglist)


  if(evolve_moves):
    arglist = zip(num_reps*[length], num_reps*[alphsize], range(num_reps))
    if(pool_size):
      pool = Pool(processes=int(pool_size))       #start the worker processes,
      r = pool.map_async(runEvolved, arglist, callback=result.append)
      r.wait()                                    #and crank on them.
    else:
      for args in arglist:
        runEvolved(args)

if __name__ == "__main__":
  #import doctest  
  #doctest.testmod()
  main()                                           
