#!/usr/bin/env python3
"""
"""

import os
import sys
import argparse

try:
  import rearrange_by_align
except:
  sys.exit('Try doing "export PYTHONPATH=src:$PYTHONPATH".')

from rearrange_by_align.genome import getGenomes
from rearrange_by_align.genome import parseGenome
from rearrange_by_align.genome import getGenome
from rearrange_by_align.breakpointgraph import BreakpointGraph


#Switches:

#Constants:
X = "X"                   #The string for genome X in the variables.
Y = "Y"                   #The string for genome Y in the variables.


#______________________________________________________________________________#
#|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#
#|:=--=:|  FUNCTIONS  |:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#

#______________________________________________________________________________#
#|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#
#|:=--=:|     Main    |:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#

#Preliminaries: __    __    __    __    __    __    __    __    __    __    __
#__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \_

def main():
  desc = 'Get the weighted DCJ distance. ' + \
         'Genomes are specified\nin UniMoG (like) file format. For example:\n\n' + \
         '  > Genome 1\n' + \
         '  1 2 3 | 4 5 )\n' + \
         '  > Genome 2\n' + \
         '  4 -2 1 )\n' + \
         '  -3 5 |\n\n' + \
         'has two genomes, each with a linear and circular chromosome.'
  parser = argparse.ArgumentParser(description=desc,
                                   formatter_class=argparse.RawDescriptionHelpFormatter)
  inputg = parser.add_argument_group('Input options')
  outputg = parser.add_argument_group('Output options')

  inputg.add_argument('MATRIX.csv',
                      help='comma delimited weight matrix.')
  inputg.add_argument('GENOME', nargs='+',
                      help='input genome filename or string (when -s is used).')
  inputg.add_argument('-s', '--string-input', action='store_true',
                      help='GENOME arguments are strings instead of fasta filenames.')

  args = parser.parse_args()

  bp_graph = args.OUTFILE
  genomes = args.GENOME
  input_strings = args.string_input

  file1 = ''
  file2 = ''
  if input_strings:
    if len(genomes) != 2:
      sys.exit(f'Error: expected exactly 2 input strings, got {len(genomes)}.')
  else:
    if len(genomes) > 2:
      sys.exit(f'Error: expected 1 or 2 genome files, got {len(genomes)}.')

    file1 = genomes[0]
    if not os.path.exists(file1):
      sys.exit("Error: file "+file1+" does not exist.\n")

    if len(genomes) == 2:
      file2 = genomes[1]
      if not os.path.exists(file2):
        sys.exit("Error: file "+file2+" does not exist.\n")

#MAIN:    __    __    __    __    __    __    __    __    __    __    __    __
#__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \_


  gx = []   #Genome 1.
  gy = []   #Genome 2.
  if input_strings:
    gx = parseGenome(genomes[0])
    gy = parseGenome(genomes[1])
  else:     #File input.
    if file2:
      _, gx = getGenome(file1)
      _, gy = getGenome(file2)
    else:
      (_, gx), (_, gy) = getGenomes(file1)

  print(f'{X}: {gx}')
  print(f'{Y}: {gy}')
  sys.stdout.flush()

  bg = BreakpointGraph(gx, gy)

  balanced, ub_black, ub_gray = bg.getBasicCycles()
  if balanced:
    print(f'Balanced cycles:\n  ', end='')
    print('\n  '.join(map(str, balanced)))
  if ub_black:
    print(f'Unbalanced black cycles:\n  ', end='')
    print('\n  '.join(map(str, ub_black)))
  if ub_gray:
    print(f'Unbalanced gray cycles:\n  ', end='')
    print('\n  '.join(map(str, ub_gray)))


if __name__ == "__main__":
  #import doctest
  #doctest.testmod()
  main()
