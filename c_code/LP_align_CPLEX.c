// *****************************************************************************
// ***      Program:  LP_align_CPLEX.c                                       ***
// ***      Version:  1.0                                                    ***
// ***       Author:  Patrick Holloway                                       ***
// ***         Date:  12-10-2011                                             ***
// ***                                                                       ***
// ***  Description:  Find the minimum cost alignment between 2 ARNt genomes ***
// ***                with these operations:                                 ***
// ***                     -match = 0                                        ***
// ***                     -duplication = 1                                  ***
// ***                     -loss = size                                      ***
// ***                The program use Integer Linear Programming to find     ***
// ***                the solution. It also finds the genome of the          ***
// ***                most recent common ancestor.                           ***
// *****************************************************************************

// **************
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include "/home/kms/cplex/cplex/include/ilcplex/cplex.h"

// **************
// Maximum number of lines for CPLEX.
// Each element in each line counts as 1 line.
// Example: "a1 + X1 + Z1 >= 1" produces 3 lines.
#define MAX_CPLEX_LINES 10000000
#define CODE_MAX_SIZE 20

// **************
// enum
enum ALIGN_OPERATION
{
  MATCH,
  LOSS_X,
  LOSS_Y,
  DUP_X,
  DUP_Y,
  TAN_X,
  TAN_Y,
  NO_OP,
  NB_OPER
};

// **************
// struct
struct FRIENDLY_LP_RESULTS_STRUCT
{
  char Xc[CODE_MAX_SIZE];
  int Xi;
  char Yc[CODE_MAX_SIZE];
  int Yi;
  enum ALIGN_OPERATION oper;
  char from_c[CODE_MAX_SIZE];
  int from_i;
  char to_c[CODE_MAX_SIZE];
  int to_i;
  char copy_from_c[CODE_MAX_SIZE];
  int copy_from_i;
  char copy_to_c[CODE_MAX_SIZE];
  int copy_to_i;
  char this_copy_c[CODE_MAX_SIZE];
  int this_copy_i;
  int dup_i;
};

struct ARNt_CODE
{
  int id;
  char code[CODE_MAX_SIZE];
  int print_no;
};

struct ALIGN_CODE
{
  char code[CODE_MAX_SIZE];
};

struct DUP_CHAIN
{
  int dup_i;
  int cycle_found_i;
};

// **************
// global variables
unsigned char verbose = 0;
unsigned char ARNt_quad_format = 0;


// **************
#define ISMATCH -1
void create_dup_dotplot(int len_T, struct ARNt_CODE T[len_T], int gr_per_elem, int ***dotplot, int *nb_groups)
{
  int i, j, k, i2, j2, i3, j3;

  // clear all at start
  for(i=0; i<len_T; i++)
    for(j=0; j<len_T; j++)
      for(k=0; k<gr_per_elem; k++)
	dotplot[i][j][k] = 0;

  // 1st pass
  for(i=0; i<len_T; i++)
  { 
    for(j=0; j<len_T; j++)
    {
      if (   (i == j)                          // Center diagonal
	  || (T[i].id != T[j].id) )                // No match
	dotplot[i][j][0] = 0;
      else                                     // Match
	dotplot[i][j][0] = ISMATCH;
    }
  }

  // 2nd pass
  *nb_groups = 0;
  for(i=0; i<len_T; i++)
  {
    for(j=0; j<len_T; j++)       //For a given non-zero cell, mark the duplications that start here.
    {
      if(dotplot[i][j][0] != 0)  // there's a duplication
      {
	// for each possible substring
	for( i2=i, j2=j; 
	    (i2<len_T) && (j2<len_T) && dotplot[i2][j2][0] && ((i2 < j) || (i > j2)); 
	     i2++, j2++)  
	{                     //Save the duplication if it does not overlap with itself:
          (*nb_groups)++;
	  for(i3=i, j3=j; (i3<=i2) && (j3<=j2); i3++, j3++)   // for each element of this substring
	  {                          // find a empty spot to write the group number
	    for(k=0; dotplot[i3][j3][k] != ISMATCH && dotplot[i3][j3][k] != 0; k++)
	    {
	      // do nothing
	    }
	    dotplot[i3][j3][k] = *nb_groups;	    
          }
	}
      }
    }
  }
}


// *************
void create_match_dotplot(int len_X, struct ARNt_CODE X[len_X], int len_Y, struct ARNt_CODE Y[len_Y], int **dotplot, int *nb_groups)
{
  int i, j;

  *nb_groups = 0;
  
  for(i=0; i<len_X; i++)
  { 
    for(j=0; j<len_Y; j++)
    {
      if(X[i].id != Y[j].id)                 // No match
      {
	dotplot[i][j] = 0;
      }
      else                                   // Match
      {
	(*nb_groups)++;
	dotplot[i][j] = *nb_groups;
      }
    }
  }
}


// **************
void print_dup_dotplot(int len_T, struct ARNt_CODE T[len_T], int len_U, struct ARNt_CODE U[len_U], int ***dotplot)
{ 
  int i, j;
  
  for(i=-1; i<len_T; i++)
  {
    if(i == -1)
      printf("   ");
    else
    {
      if(!ARNt_quad_format)
	printf(" %c ",T[i].code[0]);
      else
	printf("%2i ",T[i].print_no);
    }
    
    for(j=0; j<len_U; j++)
    {
      if(i == -1)
      {
	if(!ARNt_quad_format)
	  printf("  %c", U[j].code[0]);
	else
	  printf(" %2i", U[j].print_no);
      }
      else
      {
	printf(" %2i", dotplot[i][j][0]);
      }
    }
    printf("\n");
  }
  printf("\n");
}


// **************
void print_match_dotplot(int len_T, struct ARNt_CODE T[len_T], int len_U, struct ARNt_CODE U[len_U], int **dotplot)
{ 
  int i, j;
    
  for(i=-1; i<len_T; i++)
  {
    if(i == -1)
      printf("   ");
    else
    {
      if(!ARNt_quad_format)
	printf(" %c ",T[i].code[0]);
      else
	printf("%2i ",T[i].print_no);
    }
    
    for(j=0; j<len_U; j++)
    {
      if(i == -1)
      {
	if(!ARNt_quad_format)
	  printf("  %c", U[j].code[0]);
	else
	  printf(" %2i", U[j].print_no);
      }
      else
	printf(" %2i", dotplot[i][j]);
    }
    printf("\n");
  }
  printf("\n");
}


// **************
void show_keys(int len_X, struct ARNt_CODE X[len_X], int len_Y, struct ARNt_CODE Y[len_Y])
{
  int i;

  printf("Keys\n");
  printf("****\n");

  for(i=0; i<len_X; i++)
    printf("%2i : %s (Gen X)\n", X[i].print_no, X[i].code);

  for(i=0; i<len_Y; i++)
    printf("%2i : %s (Gen Y)\n", Y[i].print_no, Y[i].code);

  printf("\n\n");
}


// **************
void min_fct_genes(CPXENVptr env, CPXLPptr lp, int len_T, struct ARNt_CODE T[len_T], int *col, char genome_id)
{
  int i;
  double obj[len_T];
  double lb[len_T];
  double ub[len_T];
  
  if(len_T <= 0)
    return;

  if(verbose)
    if(genome_id == 'Y')
      printf(" + ");
      
  for(i=0; i<len_T; i++)
  {
    obj[i] = 1.0;
    lb[i] = 0.0;
    ub[i] = 1.0;

    if(verbose)
    {
      if(!ARNt_quad_format)
	printf("%s%i", T[i].code, *col + 1);
      else
	printf("%s_%i", T[i].code, *col + 1);
    }

    (*col)++;
    
    if((i + 1) < len_T)
    {
      if(verbose)
	printf(" + ");
    }
  }

  CPXnewcols(env, lp, len_T, obj, lb, ub, NULL, NULL);
}


// **************
void min_fct_dup(CPXENVptr env, CPXLPptr lp, int nb_groups, int *col, char genome_id)
{
  int i;
  double obj[nb_groups];
  double lb[nb_groups];
  double ub[nb_groups];
  
  if(nb_groups <= 0)
    return;
  
  if(verbose)
    printf(" + ");
  
  for(i=0; i<nb_groups; i++)
  {
    obj[i] = 1.0;
    lb[i] = 0.0;
    ub[i] = 1.0;

    (*col)++;
    
    if(verbose)
    {
      if(genome_id == 'X')
	printf("X%i", i+1);
      else 
	printf("Y%i", i+1);
    }
    
    if((i + 1) < nb_groups)
    {
      if(verbose)
	printf(" + ");
    }
  }

  CPXnewcols(env, lp, nb_groups, obj, lb, ub, NULL, NULL);
}


// **************
void min_fct_match(CPXENVptr env, CPXLPptr lp, int nb_groups, int *col)
{
  int i;
  double obj[nb_groups];
  double lb[nb_groups];
  double ub[nb_groups];
  
  if(nb_groups == 0)
    return;
    
  for(i=0; i<nb_groups; i++)
  {
    obj[i] = 0.0;  // matches are free
    lb[i] = 0.0;
    ub[i] = 1.0;

    (*col)++;
  }

  CPXnewcols(env, lp, nb_groups, obj, lb, ub, NULL, NULL);
}


// **************
void minimize_function(CPXENVptr env, CPXLPptr lp, int len_X, struct ARNt_CODE X[len_X], int len_Y, struct ARNt_CODE Y[len_Y], 
		       int dotplot_X_nb_groups, int dotplot_Y_nb_groups, int dotplot_Z_nb_groups, int *col)
{
  CPXchgobjsen(env, lp, CPX_MIN);
  
  if(verbose)
    printf("min: ");

  min_fct_genes(env, lp, len_X, X, col, 'X');
  min_fct_genes(env, lp, len_Y, Y, col, 'Y');
  min_fct_dup(env, lp, dotplot_X_nb_groups, col, 'X');
  min_fct_dup(env, lp, dotplot_Y_nb_groups, col, 'Y');
  min_fct_match(env, lp, dotplot_Z_nb_groups, col);
  
  if(verbose)
    printf(";\n");
}


// **************
void binary_constraints(CPXENVptr env, CPXLPptr lp, int *col)
{
  int i;
  char ctype[*col];

  for(i=0; i<*col; i++)
    ctype[i] = 'B';

  CPXcopyctype(env, lp, ctype);
}


// **************
void dup_and_match_constraints(CPXENVptr env, CPXLPptr lp,
			       int len_T, struct ARNt_CODE T[len_T], int dotplot_T_nb_groups,
			       int len_U, int dotplot_U_nb_groups,
			       int len_Z1, int len_Z2, int gr_per_elem, 
			       int ***dotplot_T, int **dotplot_Z, 
			       int ia[MAX_CPLEX_LINES], int ja[MAX_CPLEX_LINES], double ar[MAX_CPLEX_LINES], 
			       int *row, int *mat_i, char genome_id)
{
  int i, j, k;
  int Zi, Zj;
  char sense[1];
  double rhs[1];
    
  for(i=0; i<len_T; i++)
  {
    // loss
    ia[*mat_i] = *row;
    if(genome_id == 'X')
      ja[*mat_i] = i;
    else
      ja[*mat_i] = i + len_U;
    ar[*mat_i] = 1.0;
    (*mat_i)++;
    
    if(verbose)
    {
      if(genome_id == 'X')
      {
	if(!ARNt_quad_format)
	  printf("%s%i", T[i].code, i + 1);
	else
	  printf("%s_%i", T[i].code, i + 1);
      }
      else
      {
	if(!ARNt_quad_format)
	  printf("%s%i", T[i].code, i + 1 + len_U);
	else
	  printf("%s_%i", T[i].code, i + 1 + len_U);
      }
    }
    
    // dup
    for(j=0; j<len_T; j++)
    {
      for(k=0; (dotplot_T[i][j][k] > 0) && (k < gr_per_elem); k++)
      {
	ia[*mat_i] = *row;
	if(genome_id == 'X')
	  ja[*mat_i] = dotplot_T[i][j][k] + len_T + len_U - 1;
	else
	  ja[*mat_i] = dotplot_T[i][j][k] + len_T + len_U + dotplot_U_nb_groups - 1;
	ar[*mat_i] = 1.0;
	(*mat_i)++;
	
	if(verbose)
	{
	  if(genome_id == 'X')
	    printf(" + X%i", dotplot_T[i][j][k]);
	  else
	    printf(" + Y%i", dotplot_T[i][j][k]);
	}
      }
    }
    
    // match
    for(j=0; j<len_U; j++)
    {
      if(genome_id == 'X')
      {
	Zi = i;
	Zj = j;
      }
      else
      {
	Zi = j;
	Zj = i;
      }
      
      if(dotplot_Z[Zi][Zj] > 0)
      {
	ia[*mat_i] = *row;
	ja[*mat_i] = dotplot_Z[Zi][Zj] + len_T + len_U + dotplot_T_nb_groups + dotplot_U_nb_groups - 1;
	ar[*mat_i] = 1.0;
	(*mat_i)++;
	
	if(verbose)
	{
	  printf(" + Z%i", dotplot_Z[Zi][Zj]);
	}
      }
    }
    
    // == 1
    rhs[0] = 1.0;
    sense[0] = 'E';
    CPXnewrows(env, lp, 1, rhs, sense, NULL, NULL);
    (*row)++;
    if(verbose)
      printf(" = 1;\n");
  }  
}


// **************
void order_constraints(CPXENVptr env, CPXLPptr lp, int len_Z1, int len_Z2, int **dotplot_Z, 
		       int len_X, int len_Y, int dotplot_X_nb_groups, int dotplot_Y_nb_groups,
		       int ia[MAX_CPLEX_LINES], int ja[MAX_CPLEX_LINES], double ar[MAX_CPLEX_LINES],
		       int *row, int *mat_i)
{
  int i, j, i2, j2, temp_j;
  char sense[1];
  double rhs[1];
  
  for(i=0; i<len_Z1; i++)      // for all matrix cells
  {
    for(j=0; j<len_Z2; j++)
    {
      if(dotplot_Z[i][j] > 0)     // if there's a match
      {
	for(i2=i; i2<len_Z1; i2++)     // for all other matrix cells
	{
	  if(i2 == i)
	    temp_j = j + 1;
	  else
	    temp_j = 0;
	  
	  for(j2=temp_j; j2<len_Z2; j2++)
	  {
	    if(dotplot_Z[i2][j2] > 0)     // if there's another match
	    {
	      if(   ((i2 <= i) && (j2 >= j))    // upper-right
	         || ((i2 >= i) && (j2 <= j)) )  // lower-left
	      {
		// 1 of 2 variables constraint
		ia[*mat_i] = *row;
		ja[*mat_i] = dotplot_Z[i][j] + len_X + len_Y + dotplot_X_nb_groups + dotplot_Y_nb_groups - 1;
		ar[*mat_i] = 1.0;
		(*mat_i)++;
	    
		// 2 of 2 variables constraint
		ia[*mat_i] = *row;
		ja[*mat_i] = dotplot_Z[i2][j2] + len_X + len_Y + dotplot_X_nb_groups + dotplot_Y_nb_groups - 1;
		ar[*mat_i] = 1.0;
		(*mat_i)++;
	    
		// <= 1
		rhs[0] = 1.0;
		sense[0] = 'L';
		CPXnewrows(env, lp, 1, rhs, sense, NULL, NULL);
		(*row)++;
	    
		if(verbose)
		{
		  printf("Z%i + Z%i <= 1;\n", dotplot_Z[i][j], dotplot_Z[i2][j2]);
		}
	      }
	    }
	  }
	}
      }
    }
  }
}


// **************
void samples_constraints(CPXENVptr env, CPXLPptr lp, 
			 int len_T, struct ARNt_CODE T[len_T], int dotplot_T_nb_groups, 
			 int len_U, int dotplot_U_nb_groups,
			 int len_Z1, int len_Z2, int **dotplot_Z,
			 int ia[MAX_CPLEX_LINES], int ja[MAX_CPLEX_LINES], double ar[MAX_CPLEX_LINES],
			 int *row, int *mat_i, char genome_id)
{
  int i, j, i2;
  int Zi, Zj;
  unsigned char samples[len_T];
  char sense[1];
  double rhs[1];

  for(i=0; i<len_T; i++)
    samples[i] = 0;
  
  for(i=0; i<len_T; i++)       
  {
    if(samples[i] == 0)
    {
      ia[*mat_i] = *row;
      if(genome_id == 'X')
	ja[*mat_i] = i;
      else
	ja[*mat_i] = i + len_U;
      ar[*mat_i] = 1.0;
      (*mat_i)++;
      
      if(verbose)
      {
	if(genome_id == 'X')
	{
	  if(!ARNt_quad_format)
	    printf("%s%i", T[i].code, i + 1);
	  else
	    printf("%s_%i", T[i].code, i + 1);
	}
	else
	{
	  if(!ARNt_quad_format)
	    printf("%s%i", T[i].code, i + 1 + len_U);
	  else
	    printf("%s_%i", T[i].code, i + 1 + len_U);
	}
      }
      
      for(j=i+1; j<len_T; j++)   // find all samples of this family
      {
	if(T[i].id == T[j].id)
	{
	  ia[*mat_i] = *row;
	  if(genome_id == 'X')
	    ja[*mat_i] = j; 
	  else
	    ja[*mat_i] = j + len_U;
	  ar[*mat_i] = 1.0;
	  (*mat_i)++;
	  
	  if(verbose)
	  {
	    if(genome_id == 'X')
	    {
	      if(!ARNt_quad_format)
		printf(" + %s%i", T[j].code, j + 1);
	      else
		printf(" + %s_%i", T[j].code, j + 1);
	    }
	    else
	    {
	      if(!ARNt_quad_format)
		printf(" + %s%i", T[j].code, j + 1 + len_U);
	      else
		printf(" + %s_%i", T[j].code, j + 1 + len_U);
	    }
	  }
	  
	  samples[j] = 1;
	}
      }
      
      // match
      for(j=0; j<len_U; j++)   // for all 'Z' with the original gene
      {
	if(genome_id == 'X')
	{
	  Zi = i;
	  Zj = j;
	}
	else
	{
	  Zi = j;
	  Zj = i;
	}
	
	if(dotplot_Z[Zi][Zj] > 0)
	{
	  ia[*mat_i] = *row;
	  ja[*mat_i] = dotplot_Z[Zi][Zj] + len_T + len_U + dotplot_T_nb_groups + dotplot_U_nb_groups - 1;
	  ar[*mat_i] = 1.0;
	  (*mat_i)++;

	  if(verbose)
	  {
	    printf(" + Z%i", dotplot_Z[Zi][Zj]);
	  }
	
	  for(i2=i+1; i2<len_T; i2++)   // for all 'Z' with the copy of the gene
	  {
	    if(genome_id == 'X')
	    {
	      Zi = i2;
	      Zj = j;
	    }
	    else
	    {
	      Zi = j;
	      Zj = i2;
	    }
	    
	    if(dotplot_Z[Zi][Zj] > 0)
	    {
	      ia[*mat_i] = *row;
	      ja[*mat_i] = dotplot_Z[Zi][Zj] + len_T + len_U + dotplot_T_nb_groups + dotplot_U_nb_groups - 1;
	      ar[*mat_i] = 1.0;
	      (*mat_i)++;
    
	      if(verbose)
	      {
		printf(" + Z%i", dotplot_Z[Zi][Zj]);
	      }
	    } 
	  }
	}
      }
	
      // >= 1
      rhs[0] = 1.0;
      sense[0] = 'G';
      CPXnewrows(env, lp, 1, rhs, sense, NULL, NULL);
      (*row)++;
      if(verbose)
	printf(" >= 1;\n");
    }
  }
}


// **************
void mirror_constraints(CPXENVptr env, CPXLPptr lp, int len_T, int len_U, int dotplot_U_nb_groups, 
			int gr_per_elem, int ***dotplot_T, 
			int ia[MAX_CPLEX_LINES], int ja[MAX_CPLEX_LINES], double ar[MAX_CPLEX_LINES],
			int *row, int *mat_i, char genome_id)
{
  int i, j, k;
  char sense[1];
  double rhs[1];
  
  for(i=0; i<len_T; i++)
  {
    for(j=0; j<len_T; j++)
    {
      if(i < j)     // useless to do the work on both side of the diagonal
      {
	if((dotplot_T[i][j][0] != 0) && (dotplot_T[j][i][0] != 0))
	{
	  // bottom left side (original side) of the diagonal
	  for(k=0; (dotplot_T[i][j][k] > 0) && (k < gr_per_elem); k++)
	  {
	    ia[*mat_i] = *row;
	    if(genome_id == 'X')
	      ja[*mat_i] = dotplot_T[i][j][k] + len_T + len_U - 1;
	    else
	      ja[*mat_i] = dotplot_T[i][j][k] + len_T + len_U + dotplot_U_nb_groups - 1;
	    ar[*mat_i] = 1.0;
	    (*mat_i)++;
	    
	    if(verbose)
	    {
	      if(k != 0)
		printf(" + ");

	      if(genome_id == 'X')
		printf("X%i", dotplot_T[i][j][k]);
	      else
		printf("Y%i", dotplot_T[i][j][k]);
	    }
	  }

	  // mirror side
	  for(k=0; (dotplot_T[j][i][k] > 0) && (k < gr_per_elem); k++)
	  {	    
	    ia[*mat_i] = *row;
	    if(genome_id == 'X')
	      ja[*mat_i] = dotplot_T[j][i][k] + len_T + len_U - 1;
	    else
	      ja[*mat_i] = dotplot_T[j][i][k] + len_T + len_U + dotplot_U_nb_groups - 1;
	    ar[*mat_i] = 1.0;
	    (*mat_i)++;

	    if(verbose)
	    {
	      printf(" + ");

	      if(genome_id == 'X')
		printf("X%i", dotplot_T[j][i][k]);
	      else
		printf("Y%i", dotplot_T[j][i][k]);
	    }
	  }
	    
	  // <= 1
	  rhs[0] = 1.0;
	  sense[0] = 'L';
	  CPXnewrows(env, lp, 1, rhs, sense, NULL, NULL);
	  (*row)++;
	    
	  if(verbose)
	    printf(" <= 1;\n");
	}
      }
    }
  }
}


// **************
void print_LP_results(CPXENVptr env, CPXLPptr lp, int len_X, struct ARNt_CODE X[len_X], int len_Y, struct ARNt_CODE Y[len_Y], 
		      int dotplot_X_nb_groups, int dotplot_Y_nb_groups, int dotplot_Z_nb_groups,
		      int col, int size_LP_results, int LP_results[size_LP_results])
{
  int i;
  unsigned char first_time=1;
  
  // total cost
  printf("Total Cost = %i\n", LP_results[0]);
  
  // value of each variable
  printf("Variables: "); 
  for(i=1; i<=col; i++)
  {
    if(LP_results[i] > 0)
    {
      if(!first_time)
	printf(" ; ");
      else
	first_time = 0;
      
      if(i <= len_X)                                                                 // loss X
      {
	if(!ARNt_quad_format)
	  printf("%s%i", X[i-1].code, i);
	else
	  printf("%s_%i", X[i-1].code, i);
      }
      else if(i <= (len_X + len_Y))                                                  // loss Y
      {
	if(!ARNt_quad_format)
	  printf("%s%i", Y[i-1 - len_X].code, i);
	else
	  printf("%s_%i", Y[i-1 - len_X].code, i);
      }
      else if(i <= (len_X + len_Y + dotplot_X_nb_groups))                            // dup X
      {
	printf("X%i", i - len_X - len_Y);
      }
      else if(i <= (len_X + len_Y + dotplot_X_nb_groups + dotplot_Y_nb_groups))      // dup Y
      {
	printf("Y%i", i - len_X - len_Y - dotplot_X_nb_groups);
      }
      else                                                                           // match
      {
	printf("Z%i", i - len_X - len_Y - dotplot_X_nb_groups - dotplot_Y_nb_groups);	  
      }
    }
  }
  printf("\n\n");
}


// **************
void print_detailed_results(int size_fr_LP_results, struct FRIENDLY_LP_RESULTS_STRUCT fr_LP_results[size_fr_LP_results],
			    int total_cost, int len_align)
{
  int i;
  int old_from_X = 0, old_from_Y = 0;
  unsigned char write_flag;
  char temp[CODE_MAX_SIZE + 13];

  printf("\nDetailed Results of Linear Programming\n");
  printf("**************************************\n");
  if(!ARNt_quad_format)
  {
    printf("GenX      GenY     Operation\n");
    printf("====      ====     =========\n");
  }
  else
  {
    printf("Genome X               Genome Y              Operation\n");
    printf("========               ========              =========\n");    
  }

  for(i=0; i<len_align; i++)
  {
    if(fr_LP_results[i].Xc[0] == '-')
    {
      if(!ARNt_quad_format)
	printf(".   ");
      else
	printf(".           ");
    }
    else
    {
      if(!ARNt_quad_format)
	printf("%s%-3i", fr_LP_results[i].Xc, fr_LP_results[i].Xi);
      else
      {
	sprintf(temp, "%s_%-3i", fr_LP_results[i].Xc, fr_LP_results[i].Xi);
	printf("%-12s", temp);
      }
    }

    if(fr_LP_results[i].oper == MATCH)
    {
      if(!ARNt_quad_format)
	printf(" ---  ");
      else
	printf(" ------    ");
    }
    else
    {
      if(!ARNt_quad_format)
	printf("      ");
      else
	printf("           ");
    }
    
    if(fr_LP_results[i].Yc[0] == '-')
    {
      if(!ARNt_quad_format)
	printf(".        ");
      else
	printf(".                     ");
    }
    else
    {
      if(!ARNt_quad_format)
	printf("%s%-3i     ", fr_LP_results[i].Yc, fr_LP_results[i].Yi);
      else
      {
	sprintf(temp, "%s_%-3i", fr_LP_results[i].Yc, fr_LP_results[i].Yi);
	printf("%-12s          ", temp);
      }
    }

    write_flag = 0;
    switch(fr_LP_results[i].oper)
    {
      case MATCH:
      {
	break;
      }
      
      case LOSS_X:
      {
	if(fr_LP_results[i].from_i != old_from_Y)
	{
	  printf("Loss of ");
	  old_from_Y = fr_LP_results[i].from_i;  // consider a loss of X as an operation of Y
	  write_flag = 1;
	}
	break;
      }
      
      case LOSS_Y:
      {
	if(fr_LP_results[i].from_i != old_from_X)
	{
	  printf("Loss of ");
	  old_from_X = fr_LP_results[i].from_i;  // consider a loss of Y as an operation of X
	  write_flag = 1;
	}
	break;
      }
      
      case DUP_X:
      {
	if(fr_LP_results[i].from_i != old_from_X)
	{
	  printf("Duplication of ");
	  old_from_X = fr_LP_results[i].from_i;
	  write_flag = 2;
	}
	break;
      }
      
      case DUP_Y:
      {
	if(fr_LP_results[i].from_i != old_from_Y)
	{
	  printf("Duplication of ");
	  old_from_Y = fr_LP_results[i].from_i;
	  write_flag = 2;
	}
	break;
      }
      
      case TAN_X:
      {
	if(fr_LP_results[i].from_i != old_from_X)
	{
	  printf("Tandem duplication of ");
	  old_from_X = fr_LP_results[i].from_i;
	  write_flag = 2;
	}
	break;
      }
      
      case TAN_Y:
      {
	if(fr_LP_results[i].from_i != old_from_Y)
	{
	  printf("Tandem duplication of ");
	  old_from_Y = fr_LP_results[i].from_i;
	  write_flag = 2;
	}
	break;
      }
      
      default:
      {
	printf("???");
      }
    }
    
    if(write_flag >= 1)
    {
      if(!ARNt_quad_format)
	printf("%s%i", fr_LP_results[i].from_c, fr_LP_results[i].from_i);
      else
	printf("%s_%i", fr_LP_results[i].from_c, fr_LP_results[i].from_i);

      if(fr_LP_results[i].from_i != fr_LP_results[i].to_i)
      {
	if(!ARNt_quad_format)
	  printf(" to %s%i", fr_LP_results[i].to_c, fr_LP_results[i].to_i);
	else
	  printf(" to %s_%i", fr_LP_results[i].to_c, fr_LP_results[i].to_i);
      }
      
      if(write_flag >= 2)
      {
	printf(" copied from ");
	if(!ARNt_quad_format)
	  printf("%s%i", fr_LP_results[i].copy_from_c, fr_LP_results[i].copy_from_i);
	else
	  printf("%s_%i", fr_LP_results[i].copy_from_c, fr_LP_results[i].copy_from_i);

	if(fr_LP_results[i].copy_from_i != fr_LP_results[i].copy_to_i)
	{
	  if(!ARNt_quad_format)
	    printf(" to %s%i", fr_LP_results[i].copy_to_c, fr_LP_results[i].copy_to_i);
	  else
	    printf(" to %s_%i", fr_LP_results[i].copy_to_c, fr_LP_results[i].copy_to_i);
	}
      }
    }
    printf("\n");
  }
  printf("\nTotal Cost = %i\n\n\n", total_cost);
}


// **************
int find_nb_match(int size_LP_results, int LP_results[size_LP_results], int offset_LP_results_Z)
{
  int i;
  int nb_match = 0;
  
  for(i=offset_LP_results_Z; i<size_LP_results; i++)
  {
    if(LP_results[i] > 0)
      nb_match++;
  }
  
  return(nb_match);
}


// **************
void edit_align(int len_X, struct ARNt_CODE X[len_X], int len_Y, struct ARNt_CODE Y[len_Y], int **dotplot_Z,
	        int size_LP_results, int LP_results[size_LP_results], int offset_LP_results_Z,
	        int len_align, struct ALIGN_CODE align_X[len_align + 1], struct ALIGN_CODE align_Y[len_align + 1])
{
  int i, j1, j2, j3, k;
  int ind_x, ind_y;
  unsigned char found;
  
  ind_x = 0;
  ind_y = 0;
  j1 = 0;
  for(i=0; i<len_X; i++)
  {
    found = 0;
    for(j2=j1; j2<len_Y && !found; j2++)
    {
      k = dotplot_Z[i][j2];
      if(k > 0)
      {
	if(LP_results[offset_LP_results_Z + k - 1] > 0)
	{
	  // match of X[i] with Y[j2]
	  for(j3=j1; j3<j2; j3++)
	  {
	    strcpy(align_X[ind_x].code, "-\0");
	    ind_x++;
	    strcpy(align_Y[ind_y].code, Y[j3].code);
	    ind_y++;
	  }
	  strcpy(align_X[ind_x].code, X[i].code);
	  ind_x++;
	  strcpy(align_Y[ind_y].code, Y[j2].code);
	  ind_y++;
	  j1 = j2 + 1;
	  found = 1;
	}
      }
    }
    if(!found)   // no match
    {
      strcpy(align_X[ind_x].code, X[i].code);
      ind_x++;
      strcpy(align_Y[ind_y].code, "-\0");
      ind_y++;
    }
  }
  // at the end, fill the rest of Y
  for(j3=j1; j3<len_Y; j3++)
  {
    strcpy(align_X[ind_x].code, "-\0");
    ind_x++;
    strcpy(align_Y[ind_y].code, Y[j3].code);
    ind_y++;
  }
  
  // add the end of string
  align_X[ind_x].code[0] = '\0';
  align_Y[ind_y].code[0] = '\0';
}


// **************
void close_sequence(int len_align, struct FRIENDLY_LP_RESULTS_STRUCT fr_LP_results[len_align], enum ALIGN_OPERATION prev_oper,
		    int i, int start_ind, char end_c[CODE_MAX_SIZE], int end_i, char end_copy_c[CODE_MAX_SIZE], int end_copy_i, int start_i, int start_copy_i,
		    char genome_id)
{
  int j;

  for(j=start_ind; j<i; j++)
  {
    if( ((genome_id == 'X') && (fr_LP_results[j].Xc[0] != '-')) ||
        ((genome_id == 'Y') && (fr_LP_results[j].Yc[0] != '-')) )
    {
      strcpy(fr_LP_results[j].to_c, end_c);
      fr_LP_results[j].to_i = end_i;
    }
  }

  if((prev_oper == DUP_X) || (prev_oper == DUP_Y))
  {
    for(j=start_ind; j<i; j++)
    {
      strcpy(fr_LP_results[j].copy_to_c, end_copy_c);
      fr_LP_results[j].copy_to_i = end_copy_i;
      
      if( ((end_copy_i + 1) == start_i) || ((end_i + 1) == start_copy_i) )
      {
	if(genome_id == 'X')
	  fr_LP_results[j].oper = TAN_X;
	else
	  fr_LP_results[j].oper = TAN_Y;
      }
    }
  }
}


// **************
unsigned char produce_friendly_LP_results(int len_align, struct ALIGN_CODE align_X[len_align + 1], struct ALIGN_CODE align_Y[len_align + 1], 
					  int len_X, struct ARNt_CODE X[len_X], int len_Y, struct ARNt_CODE Y[len_Y],
					  int gr_per_elem_X, int gr_per_elem_Y,
		        		  int ***dotplot_X, 
					  int ***dotplot_Y, 
					  int **dotplot_Z,
				          int dotplot_X_nb_groups, int size_LP_results, int LP_results[size_LP_results], 
				          struct FRIENDLY_LP_RESULTS_STRUCT fr_LP_results[len_align])
{
  int i, j, k;
  int ind_x, ind_y;
  unsigned char end;
  int dup, old_dup_X = 0, old_dup_Y = 0;
  char start_Xc[CODE_MAX_SIZE] = "\0", end_Xc[CODE_MAX_SIZE] = "\0", start_Yc[CODE_MAX_SIZE] = "\0", end_Yc[CODE_MAX_SIZE] = "\0";
  char start_copy_Xc[CODE_MAX_SIZE] = "\0", end_copy_Xc[CODE_MAX_SIZE] = "\0", start_copy_Yc[CODE_MAX_SIZE] = "\0", end_copy_Yc[CODE_MAX_SIZE] = "\0", copy_c[CODE_MAX_SIZE];
  int start_Xi = 0, end_Xi = 0, start_Yi = 0, end_Yi = 0;
  int start_copy_Xi = 0, end_copy_Xi = 0, start_copy_Yi = 0, end_copy_Yi = 0, copy_i = 0;
  int start_ind_X, start_ind_Y;
  enum ALIGN_OPERATION prev_oper;
  
  // init
  for(i=0; i<len_align; i++)
  {
    fr_LP_results[i].Xc[0] = '\0';
    fr_LP_results[i].Xi = 0;
    fr_LP_results[i].Yc[0] = '\0';
    fr_LP_results[i].Yi = 0;
    fr_LP_results[i].oper = 0;
    fr_LP_results[i].from_c[0] = '\0';
    fr_LP_results[i].from_i = 0;
    fr_LP_results[i].to_c[0] = '\0';
    fr_LP_results[i].to_i = 0;
    fr_LP_results[i].copy_from_c[0] = '\0';
    fr_LP_results[i].copy_from_i = 0;
    fr_LP_results[i].copy_to_c[0] = '\0';
    fr_LP_results[i].copy_to_i = 0;
    fr_LP_results[i].this_copy_c[0] = '\0';
    fr_LP_results[i].this_copy_i = 0;
    fr_LP_results[i].dup_i = 0;
  }
  
  prev_oper = NO_OP;
  start_ind_X = 0;
  start_ind_Y = 0;
  
  // for each element of the alignment
  ind_x = 0;
  ind_y = len_X;
  for(i=0; i<len_align; i++)
  {
    strcpy(fr_LP_results[i].Xc, align_X[i].code);
    if(align_X[i].code[0] != '-')
    {
      ind_x++;
      fr_LP_results[i].Xi = ind_x;
    }
    
    strcpy(fr_LP_results[i].Yc, align_Y[i].code);
    if(align_Y[i].code[0] != '-')
    {
      ind_y++;
      fr_LP_results[i].Yi = ind_y;
    }
    
    if((align_X[i].code[0] != '-') && (align_Y[i].code[0] != '-'))                   // MATCH
    {
      // close sequence
      if((prev_oper == LOSS_Y) || (prev_oper == DUP_X))
	close_sequence(len_align, fr_LP_results, prev_oper, i, start_ind_X, end_Xc, end_Xi, end_copy_Xc, end_copy_Xi, start_Xi, start_copy_Xi, 'X');
      else if((prev_oper == LOSS_X) || (prev_oper == DUP_Y))
	close_sequence(len_align, fr_LP_results, prev_oper, i, start_ind_Y, end_Yc, end_Yi, end_copy_Yc, end_copy_Yi, start_Yi, start_copy_Yi, 'Y');
	
      fr_LP_results[i].oper = MATCH;
      prev_oper = MATCH;
    }
    else
    {
      if(align_X[i].code[0] != '-')   // operation on X only
      {
	if(LP_results[ind_x] == 1)    // LOSS_Y  (consider it like an insertion in X)
	{
	  // close sequence
	  if(prev_oper == DUP_X)
	    close_sequence(len_align, fr_LP_results, prev_oper, i, start_ind_X, end_Xc, end_Xi, end_copy_Xc, end_copy_Xi, start_Xi, start_copy_Xi, 'X');
	  else if((prev_oper == LOSS_X) || (prev_oper == DUP_Y))
	    close_sequence(len_align, fr_LP_results, prev_oper, i, start_ind_Y, end_Yc, end_Yi, end_copy_Yc, end_copy_Yi, start_Yi, start_copy_Yi, 'Y');
	  
	  // open sequence
	  if(prev_oper != LOSS_Y)
	  {
	    strcpy(start_Xc, align_X[i].code);
	    start_Xi = ind_x;
	    start_ind_X = i;
	  }
	
	  // continue sequence
	  fr_LP_results[i].oper = LOSS_Y;                            
	  strcpy(fr_LP_results[i].from_c, start_Xc);
	  fr_LP_results[i].from_i = start_Xi;
	  strcpy(end_Xc, align_X[i].code);
	  end_Xi = ind_x;
	  prev_oper = LOSS_Y;
	}
	else                          // DUP_X or TAN_X
	{
	  end = 0;
	  for(j=0; j<len_X && !end; j++)
	  {
	    k = 0;
	    dup = dotplot_X[ind_x - 1][j][k];
	    while(dup!=0 && !end)
	    {
	      if(LP_results[len_X + len_Y + dup] == 1)
	      {
		strcpy(copy_c, X[j].code);
		copy_i = j + 1;
		end = 1;
	      }
	      else
	      {
		if(k < (gr_per_elem_X - 1))
		{ 
		  k++;
		  dup = dotplot_X[ind_x - 1][j][k];
		}
		else
		  dup = 0;
	      }
	    }
	  }
	  if(!end)
	  {
	    printf("\n\nERROR: UNKNOWN OPERATION!\n");
	    return(0);
	  }
		
	  // close sequence
	  if((prev_oper == LOSS_Y) || ((prev_oper == DUP_X) && (dup != old_dup_X)) )
	    close_sequence(len_align, fr_LP_results, prev_oper, i, start_ind_X, end_Xc, end_Xi, end_copy_Xc, end_copy_Xi, start_Xi, start_copy_Xi, 'X');
	  else if((prev_oper == LOSS_X) || (prev_oper == DUP_Y))
	    close_sequence(len_align, fr_LP_results, prev_oper, i, start_ind_Y, end_Yc, end_Yi, end_copy_Yc, end_copy_Yi, start_Yi, start_copy_Yi, 'Y');
	  
	  // open sequence
	  if(!( (prev_oper == DUP_X) && (dup == old_dup_X) ))      
	  {
	    strcpy(start_Xc, align_X[i].code);
	    start_Xi = ind_x;
	    start_ind_X = i;
	    old_dup_X = dup;
	    strcpy(start_copy_Xc, copy_c);
	    start_copy_Xi = copy_i;
	  }
	  
	  // continue sequence
	  fr_LP_results[i].oper = DUP_X;                            
	  strcpy(fr_LP_results[i].from_c, start_Xc);
	  fr_LP_results[i].from_i = start_Xi;
	  strcpy(fr_LP_results[i].copy_from_c, start_copy_Xc);
	  fr_LP_results[i].copy_from_i = start_copy_Xi;
	  strcpy(fr_LP_results[i].this_copy_c, copy_c);
	  fr_LP_results[i].this_copy_i = copy_i;
	  fr_LP_results[i].dup_i = dup;
	  strcpy(end_Xc, align_X[i].code);
	  end_Xi = ind_x;
	  strcpy(end_copy_Xc, copy_c);
	  end_copy_Xi = copy_i;
	  prev_oper = DUP_X;
	}
      }
      else   // operation on Y only
      {
	if(LP_results[ind_y] == 1)   // LOSS_X  (consider it like an insertion in Y)
	{

	  // close sequence
	  if((prev_oper == LOSS_Y) || (prev_oper == DUP_X))
	    close_sequence(len_align, fr_LP_results, prev_oper, i, start_ind_X, end_Xc, end_Xi, end_copy_Xc, end_copy_Xi, start_Xi, start_copy_Xi, 'X');
	  else if(prev_oper == DUP_Y)
	    close_sequence(len_align, fr_LP_results, prev_oper, i, start_ind_Y, end_Yc, end_Yi, end_copy_Yc, end_copy_Yi, start_Yi, start_copy_Yi, 'Y');
	    
	  // open sequence
	  if(prev_oper != LOSS_X)                                  
	  {
	    strcpy(start_Yc, align_Y[i].code);
	    start_Yi = ind_y;
	    start_ind_Y = i;
	  }
	
	  // continue sequence
	  fr_LP_results[i].oper = LOSS_X;                           
	  strcpy(fr_LP_results[i].from_c, start_Yc);
	  fr_LP_results[i].from_i = start_Yi;
	  strcpy(end_Yc, align_Y[i].code);
	  end_Yi = ind_y;
	  prev_oper = LOSS_X;
	}
	else   // DUP_Y or TAN_Y
	{
	  end = 0;
	  for(j=0; j<len_Y && !end; j++)
	  {
	    k = 0;
	    dup = dotplot_Y[ind_y - len_X - 1][j][k];
	    while(dup!=0 && !end)
	    {
	      if(LP_results[len_X + len_Y + dotplot_X_nb_groups + dup] == 1)
	      {
		strcpy(copy_c, Y[j].code);
		copy_i = j + 1 + len_X;
		end = 1;
	      }
	      else
	      {
		if(k < (gr_per_elem_Y - 1))
		{ 
		  k++;
		  dup = dotplot_Y[ind_y - len_X - 1][j][k];
		}
		else
		  dup = 0;
	      }
	    }
	  }
	  if(!end)
	  {
	    printf("\n\nERROR: UNKNOWN OPERATION!\n");
	    return(0);
	  }
		
	  // close sequence
	  if((prev_oper == LOSS_Y) || (prev_oper == DUP_X))
	    close_sequence(len_align, fr_LP_results, prev_oper, i, start_ind_X, end_Xc, end_Xi, end_copy_Xc, end_copy_Xi, start_Xi, start_copy_Xi, 'X');
	  else if((prev_oper == LOSS_X) || ((prev_oper == DUP_Y) && (dup != old_dup_Y)) ) 
	    close_sequence(len_align, fr_LP_results, prev_oper, i, start_ind_Y, end_Yc, end_Yi, end_copy_Yc, end_copy_Yi, start_Yi, start_copy_Yi, 'Y');
	  
	  // open sequence
	  if(!( (prev_oper == DUP_Y) && (dup == old_dup_Y) ))      
	  {
	    strcpy(start_Yc, align_Y[i].code);
	    start_Yi = ind_y;
	    start_ind_Y = i;
	    old_dup_Y = dup;
	    strcpy(start_copy_Yc, copy_c);
	    start_copy_Yi = copy_i;
	  }
	  
	  // continue sequence
	  fr_LP_results[i].oper = DUP_Y;                            
	  strcpy(fr_LP_results[i].from_c, start_Yc);
	  fr_LP_results[i].from_i = start_Yi;
	  strcpy(fr_LP_results[i].copy_from_c, start_copy_Yc);
	  fr_LP_results[i].copy_from_i = start_copy_Yi;
	  strcpy(fr_LP_results[i].this_copy_c, copy_c);
	  fr_LP_results[i].this_copy_i = copy_i;
	  fr_LP_results[i].dup_i = dup;
	  strcpy(end_Yc, align_Y[i].code);
	  end_Yi = ind_y;
	  strcpy(end_copy_Yc, copy_c);
	  end_copy_Yi = copy_i;
	  prev_oper = DUP_Y;
	}
      }
    }
  }
	
  // be sure to close any pending sequences
  if((prev_oper == LOSS_Y) || (prev_oper == DUP_X))
    close_sequence(len_align, fr_LP_results, prev_oper, i, start_ind_X, end_Xc, end_Xi, end_copy_Xc, end_copy_Xi, start_Xi, start_copy_Xi, 'X');  
  if((prev_oper == LOSS_X) || (prev_oper == DUP_Y))
    close_sequence(len_align, fr_LP_results, prev_oper, i, start_ind_Y, end_Yc, end_Yi, end_copy_Yc, end_copy_Yi, start_Yi, start_copy_Yi, 'Y');
  
  return(1);
}


// **************
void find_ancestor(int size, struct FRIENDLY_LP_RESULTS_STRUCT fr_LP_results[size], struct ALIGN_CODE align_ancestor[size + 1], int len_align)
{
  int i;
  
  for(i=0; i<len_align; i++)
  {
    if(   (fr_LP_results[i].oper != DUP_X)
       && (fr_LP_results[i].oper != DUP_Y)
       && (fr_LP_results[i].oper != TAN_X)
       && (fr_LP_results[i].oper != TAN_Y) )
    {
      if(fr_LP_results[i].Xc[0] != '-')
	strcpy(align_ancestor[i].code, fr_LP_results[i].Xc);
      else
	strcpy(align_ancestor[i].code, fr_LP_results[i].Yc);
    }
    else
    {
      sprintf(align_ancestor[i].code, "-");
    }
  }
  align_ancestor[i].code[0] = '\0';
}


// **************
void print_summary_results_classic(int size, struct ALIGN_CODE align_X[size], struct ALIGN_CODE align_Y[size], 
				   struct ALIGN_CODE align_ancestor[size], int len_align)
{
  int i;

  printf("Results Summary\n");
  printf("***************\n");
  printf("Genome X: ");
  for(i=0; i<len_align; i++)
    printf("%s", align_X[i].code);
  printf("\n");
  printf("Genome Y: ");
  for(i=0; i<len_align; i++)
    printf("%s", align_Y[i].code);
  printf("\n");
  printf("Ancestor: ");
  for(i=0; i<len_align; i++)
    printf("%s", align_ancestor[i].code);
  printf("\n\n");
}


// **************
void print_summary_results_quads(int size, struct ALIGN_CODE align_X[size], struct ALIGN_CODE align_Y[size], 
				 struct ALIGN_CODE align_ancestor[size], int len_align, unsigned char detailed_results)
{
  int i;
  unsigned char first;

  printf("Results Summary\n");
  printf("***************\n");
  
  printf("Genome X: ");
  first = 1;
  for(i=0; i<len_align; i++)
  {
    if(align_X[i].code[0] != '-')
    {
      if(first)
      {
	printf("%s", align_X[i].code);
	first = 0;
      }
      else
	printf(" , %s", align_X[i].code);
    }
  }
  printf("\n");

  printf("Genome Y: ");
  first = 1;
  for(i=0; i<len_align; i++)
  {
    if(align_Y[i].code[0] != '-')
    {
      if(first)
      {
	printf("%s", align_Y[i].code);
	first = 0;
      }
      else
	printf(" , %s", align_Y[i].code);
    }
  }
  printf("\n");

  printf("Ancestor: ");
  first = 1;
  for(i=0; i<len_align; i++)
  {
    if(align_ancestor[i].code[0] != '-')
    {
      if(first)
      {
	printf("%s", align_ancestor[i].code);
	first = 0;
      }
      else
	printf(" , %s", align_ancestor[i].code);
    }
  }
  printf("\n\n");

  if(detailed_results)
    printf("See the detailed results above for the alignment.\n\n");
  else
    printf("Run the program with '-d' to have the alignment.\n\n");
}


// **************
int find_cycles(CPXENVptr env, CPXLPptr lp, int len_X, int len_Y, int dotplot_X_nb_groups, int dotplot_Y_nb_groups,
		struct FRIENDLY_LP_RESULTS_STRUCT fr_LP_results[len_X + len_Y], int len_align, 
		int ia[MAX_CPLEX_LINES], int ja[MAX_CPLEX_LINES], double ar[MAX_CPLEX_LINES], 
		int *row, int *mat_i)
{
  int nb_cycles;
  int cycle_found_i;
  int cycle_found_X[len_X];
  int cycle_found_Y[len_Y];
  struct DUP_CHAIN dup_chain_X[dotplot_X_nb_groups];
  struct DUP_CHAIN dup_chain_Y[dotplot_Y_nb_groups];
  int chain_i;
  int this_copy_i;
  int fr_LP_i;
  unsigned char found;
  unsigned char end_of_chain;
  int nb_elem_in_chain;
  int i, j, k;
  char sense[1];
  double rhs[1];
  
  //init
  for(i=0; i<len_X; i++)
    cycle_found_X[i] = 0;
  for(i=0; i<len_Y; i++)
    cycle_found_Y[i] = 0;

  nb_cycles = 0;
  
  // Genome X
  for(i=0; i<len_align; i++)                                   // for each element of the genome
  {
    if(fr_LP_results[i].Xc[0] != '-')
    {
      cycle_found_i = fr_LP_results[i].Xi - 1;
      if(cycle_found_X[cycle_found_i] != 1)                    // cycle NOT already found
      {
	if((fr_LP_results[i].oper == DUP_X) ||
	   (fr_LP_results[i].oper == TAN_X))
	{
	  chain_i = 0;
	  dup_chain_X[chain_i].dup_i = fr_LP_results[i].dup_i;
	  dup_chain_X[chain_i].cycle_found_i = cycle_found_i;
	  chain_i++;
	  end_of_chain = 0;
	  this_copy_i = fr_LP_results[i].this_copy_i;
	  while(!end_of_chain)                                 // follow the duplication chain
	  {
	    found = 0;
	    for(j=0; (j<len_align) && !found; j++)
	    {
	      if(fr_LP_results[j].Xi == this_copy_i)
	      {
		cycle_found_i = fr_LP_results[j].Xi - 1;
		fr_LP_i = j;
		found = 1;
	      }
	    }
	    if(!found)
	    {
	      printf("\n\nERROR: Cannot find this duplication: X%i\n", this_copy_i);
	      return(-1);
	    }

	    if(cycle_found_X[cycle_found_i] == 1)              // cycle already found - end chain
	    {
	      end_of_chain = 1;
	    }
	    else
	    {
	      if((fr_LP_results[fr_LP_i].oper != DUP_X) &&
		 (fr_LP_results[fr_LP_i].oper != TAN_X))       // not a cycle - end chain
	      {
		end_of_chain = 1;
	      }
	      else
	      {
		for(j=0; (j<chain_i) && !end_of_chain; j++)    // check our chain if there's a cycle
		{
		  if(dup_chain_X[j].dup_i == fr_LP_results[fr_LP_i].dup_i)   // it's a cycle - end chain
		  {
		    if(verbose)
		      printf("Adding constraint:");

		    nb_elem_in_chain = 0;
		    for(k=j; k<chain_i; k++)
		    {
		      ia[*mat_i] = *row;
		      ja[*mat_i] = dup_chain_X[k].dup_i + len_X + len_Y - 1;
		      ar[*mat_i] = 1.0;
		      (*mat_i)++;
		      nb_elem_in_chain++;

		      if(verbose)
		      {
			if(nb_elem_in_chain > 1)
			  printf(" +");
			printf(" X%i", dup_chain_X[k].dup_i);
		      }

		      cycle_found_i = dup_chain_X[k].cycle_found_i;
		      cycle_found_X[cycle_found_i] = 1;
		    }
	    
		    // < nb_elem_in_chain
		    rhs[0] = (double)(nb_elem_in_chain - 1);
		    sense[0] = 'L';
		    CPXnewrows(env, lp, 1, rhs, sense, NULL, NULL);
		    (*row)++;
		    
		    if(verbose)
		      printf(" <= %i\n", nb_elem_in_chain - 1);
		    
		    end_of_chain = 1;
		    nb_cycles++;
		  }
		}
	      }

	      if(!end_of_chain)                                // not a cycle for now - continue chain
	      {
		dup_chain_X[chain_i].dup_i = fr_LP_results[fr_LP_i].dup_i;
		dup_chain_X[chain_i].cycle_found_i = cycle_found_i; 
		chain_i++;
		this_copy_i = fr_LP_results[fr_LP_i].this_copy_i;
	      }
	    }
	  }
	}
      }
    }
  }
  
  // Genome Y
  for(i=0; i<len_align; i++)                                   // for each element of the genome
  {
    if(fr_LP_results[i].Yc[0] != '-')
    {
      cycle_found_i = fr_LP_results[i].Yi - 1 - len_X;      
      if(cycle_found_Y[cycle_found_i] != 1)
      {
	if((fr_LP_results[i].oper == DUP_Y) ||
	   (fr_LP_results[i].oper == TAN_Y))
	{
	  chain_i = 0;
	  dup_chain_Y[chain_i].dup_i = fr_LP_results[i].dup_i;
	  dup_chain_Y[chain_i].cycle_found_i = cycle_found_i;
	  chain_i++;
	  end_of_chain = 0;
	  this_copy_i = fr_LP_results[i].this_copy_i;
	  while(!end_of_chain)                                 // follow the duplication chain
	  {
	    found = 0;
	    for(j=0; (j<len_align) && !found; j++)
	    {
	      if(fr_LP_results[j].Yi == this_copy_i)
	      {
		cycle_found_i = fr_LP_results[j].Yi - 1 - len_X;
		fr_LP_i = j;
		found = 1;
	      }
	    }
	    if(!found)
	    {
	      printf("\n\nERROR: Cannot find this duplication: Y%i\n", this_copy_i);
	      return(-1);
	    }
	    
	    if(cycle_found_Y[cycle_found_i] == 1)            // cycle already found - end chain
	    {
	      end_of_chain = 1;
	    }
	    else
	    {
	      if((fr_LP_results[fr_LP_i].oper != DUP_Y) &&
		 (fr_LP_results[fr_LP_i].oper != TAN_Y))               // not a cycle - end chain
	      {
		end_of_chain = 1;
	      }
	      else
	      {
		for(j=0; (j<chain_i) && !end_of_chain; j++)            // check our chain if there's a cycle
		{
		  if(dup_chain_Y[j].dup_i == fr_LP_results[fr_LP_i].dup_i)   // it's a cycle - end chain
		  {
		    if(verbose)
		      printf("Adding constraint:");

		    nb_elem_in_chain = 0;
		    for(k=j; k<chain_i; k++)
		    {
		      ia[*mat_i] = *row;
		      ja[*mat_i] = dup_chain_Y[k].dup_i + len_X + len_Y + dotplot_X_nb_groups - 1;
		      ar[*mat_i] = 1.0;
		      (*mat_i)++;
		      nb_elem_in_chain++;

		      if(verbose)
		      {
			if(nb_elem_in_chain > 1)
			  printf(" +");
			printf(" Y%i", dup_chain_Y[k].dup_i);
		      }
		      
		      cycle_found_i = dup_chain_Y[k].cycle_found_i;
		      cycle_found_Y[cycle_found_i] = 1;
		    }
	    
		    // < nb_elem_in_chain
		    rhs[0] = (double)(nb_elem_in_chain - 1);
		    sense[0] = 'L';
		    CPXnewrows(env, lp, 1, rhs, sense, NULL, NULL);
		    (*row)++;
		  
		    if(verbose)
		      printf(" <= %i\n", nb_elem_in_chain - 1);
		
		    end_of_chain = 1;
		    nb_cycles++;
		  }
		}
	      }
	      
	      if(!end_of_chain)                                      // not a cycle for now - continue chain
	      {
		dup_chain_Y[chain_i].dup_i = fr_LP_results[fr_LP_i].dup_i;
		dup_chain_Y[chain_i].cycle_found_i = cycle_found_i;
		chain_i++;
		this_copy_i = fr_LP_results[fr_LP_i].this_copy_i;
	      }
	    } 
	  }
	}
      }
    }
  }
  
  if(verbose)
    printf("\n");

  return(nb_cycles);
}


// **************
unsigned char resolve_LP(int len_X, struct ARNt_CODE X[len_X], int len_Y, struct ARNt_CODE Y[len_Y], 
			 int gr_per_elem_X, int gr_per_elem_Y,
			 int ***dotplot_X, int dotplot_X_nb_groups, 
			 int ***dotplot_Y, int dotplot_Y_nb_groups, 
			 int **dotplot_Z, int dotplot_Z_nb_groups,
			 int size_LP_results, int LP_results[size_LP_results], 
			 unsigned char running_time,
			 struct FRIENDLY_LP_RESULTS_STRUCT fr_LP_results[len_X + len_Y], 
			 struct ALIGN_CODE align_X[len_X + len_Y], struct ALIGN_CODE align_Y[len_X + len_Y], int *len_align)
{
  CPXENVptr env = NULL;
  CPXLPptr lp = NULL;
  int status = 0;

  int *ia;
  int *ja;
  double *ar;
  int row, col, mat_i;
  int round_no;
  int len_Z1 = len_X;
  int len_Z2 = len_Y;  
  int i;
  time_t start_time, end_time, real_start_time = 0;
  struct tm *timeinfo;
  double elapsed;
  int elapsed_hour, elapsed_min, elapsed_sec;
  int nb_match;
  int nb_cycles;
  double objval;

  // init LP
  env = CPXopenCPLEX(&status);
  if(env == NULL)
  {
    printf("\n!!! ERROR: Cannot open CPLEX environment !!!\n\n\n");
    return(0);
  }

  lp = CPXcreateprob(env, &status, "lpex1");
  if(lp == NULL)
  {
    printf("\n!!! ERROR: Failed to create LP !!!\n\n\n");
    return(0);
  }

  // CPXsetintparam(env, CPX_PARAM_SCRIND, CPX_ON);

  ia = (int*)malloc(MAX_CPLEX_LINES * sizeof(int));
  ja = (int*)malloc(MAX_CPLEX_LINES * sizeof(int));
  ar = (double*)malloc(MAX_CPLEX_LINES * sizeof(double));
  col = 0;
  row = 0;
  mat_i = 0;
  
  if(verbose)
  {
    printf("Linear Programming Objective Function & Constraints\n");
    printf("***************************************************\n");
  }


  // *************************
  // *** MINIMIZE FUNCTION ***
  minimize_function(env, lp, len_X, X, len_Y, Y, dotplot_X_nb_groups, dotplot_Y_nb_groups, dotplot_Z_nb_groups, &col);


  // ******************************
  // *** ESSENTIALS CONSTRAINTS ***
  // (necessary to get the right result)
  
  // all columns equal to 0 or 1
  binary_constraints(env, lp, &col);
  
  // each gene most come from something
  dup_and_match_constraints(env, lp, len_X, X, dotplot_X_nb_groups, len_Y, dotplot_Y_nb_groups, len_Z1, len_Z2, gr_per_elem_X, dotplot_X, dotplot_Z, ia, ja, ar, &row, &mat_i, 'X');
  dup_and_match_constraints(env, lp, len_Y, Y, dotplot_Y_nb_groups, len_X, dotplot_X_nb_groups, len_Z1, len_Z2, gr_per_elem_Y, dotplot_Y, dotplot_Z, ia, ja, ar, &row, &mat_i, 'Y');

  // order of each genome must be respected in the alignment
  order_constraints(env, lp, len_Z1, len_Z2, dotplot_Z, len_X, len_Y, dotplot_X_nb_groups, dotplot_Y_nb_groups, ia, ja, ar, &row, &mat_i);

  
  // ****************************
  // *** OPTIONAL CONSTRAINTS ***
  // (to lower the number of rounds by avoiding circular duplications)
  
  // at least 1 sample of each genes family constraints
  samples_constraints(env, lp, len_X, X, dotplot_X_nb_groups, len_Y, dotplot_Y_nb_groups, len_Z1, len_Z2, dotplot_Z, ia, ja, ar, &row, &mat_i, 'X');
  samples_constraints(env, lp, len_Y, Y, dotplot_Y_nb_groups, len_X, dotplot_X_nb_groups, len_Z1, len_Z2, dotplot_Z, ia, ja, ar, &row, &mat_i, 'Y');

  // avoid simple circular duplications
  mirror_constraints(env, lp, len_X, len_Y, 0,                   gr_per_elem_X, dotplot_X, ia, ja, ar, &row, &mat_i, 'X');
  mirror_constraints(env, lp, len_Y, len_X, dotplot_X_nb_groups, gr_per_elem_Y, dotplot_Y, ia, ja, ar, &row, &mat_i, 'Y');

  if(verbose)
    printf("\n");

  
  // *******************************
  // *** RESOLVE INTEGER SIMPLEX ***
  printf("\n");
  nb_cycles = -1;
  round_no = 1;
  
  while(nb_cycles != 0)
  {
    if(running_time)
    {
      time(&start_time);
      timeinfo = localtime(&start_time);
      printf("Round #%i - Starting Integer Simplex Resolution at %i:%02i:%02i  (%02i-%02i-%04i)\n", 
	     round_no, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, 
	     timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900);
      if(nb_cycles == -1)
	real_start_time = start_time;
    }

    // fill constraints
    CPXchgcoeflist(env, lp, mat_i, ia, ja, ar);
    
    // call MIP solver
    status = CPXmipopt(env, lp);
    if(status)
    {
      printf("\n!!! ERROR: Failed to optimize MIP !!!\n\n\n");
      CPXfreeprob(env, &lp);
      CPXcloseCPLEX(&env);
      return(0);
    }    

    // get total cost
    status = CPXgetobjval(env, lp, &objval);
    if(status)
    {
      printf("\n!!! ERROR: Failed to get MIP objective value!!!\n\n\n");
      CPXfreeprob(env, &lp);
      CPXcloseCPLEX(&env);
      return(0);
    }
    LP_results[0] = (int)(objval + 0.5);

    // get all variables
    {
      double variables[col];
      status = CPXgetx(env, lp, variables, 0, col-1);
      if(status)
      {
	printf("\n!!! ERROR: Failed to get optimal integer variables!!!\n\n\n");
	CPXfreeprob(env, &lp);
	CPXcloseCPLEX(&env);
	return(0);
      }
      for(i=0; i<col; i++)
	LP_results[i+1] = (int)(variables[i] + 0.5);
    }

    if(running_time)
    {
      time(&end_time);
      timeinfo = localtime(&end_time);
      printf("Round #%i - Ending Integer Simplex Resolution at %i:%02i:%02i  (%02i-%02i-%04i)\n", 
	     round_no, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec,
	     timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900);
      elapsed = difftime(end_time, start_time);
      elapsed_hour = elapsed / 3600;
      elapsed_min = (elapsed - (elapsed_hour * 3600)) / 60;
      elapsed_sec = (elapsed - (elapsed_hour * 3600) - (elapsed_min * 60));
      printf("Round #%i - Integer Simplex Resolution took %i hour(s), %02i minute(s) and %02i second(s)\n\n", 
	     round_no, elapsed_hour, elapsed_min, elapsed_sec);
    }

    // print results
    if(verbose)
    {
      printf("Results of Linear Programming  ---  Round #%i\n", round_no);
      printf("********************************************\n");
      print_LP_results(env, lp, len_X, X, len_Y, Y, dotplot_X_nb_groups, dotplot_Y_nb_groups, dotplot_Z_nb_groups, col, size_LP_results, LP_results);
    }
  
    nb_match = find_nb_match(size_LP_results, LP_results, 
			     1 + len_X + len_Y + dotplot_X_nb_groups + dotplot_Y_nb_groups);  
    *len_align = len_X + len_Y - nb_match;

    edit_align(len_X, X, len_Y, Y, dotplot_Z, 
	       size_LP_results, LP_results,
	       1 + len_X + len_Y + dotplot_X_nb_groups + dotplot_Y_nb_groups,
	       *len_align, align_X, align_Y);
  
    if(!produce_friendly_LP_results(*len_align, align_X, align_Y, len_X, X, len_Y, Y, gr_per_elem_X, gr_per_elem_Y, 
				    dotplot_X, dotplot_Y, dotplot_Z, 
				    dotplot_X_nb_groups, size_LP_results, LP_results, fr_LP_results))
    {
      printf("\n!!! ERROR: Cannot produce LP results !!!\n\n\n");
      CPXfreeprob(env, &lp);
      CPXcloseCPLEX(&env);
      return(0);
    }

    // check for cycles
    nb_cycles = find_cycles(env, lp, len_X, len_Y, dotplot_X_nb_groups, dotplot_Y_nb_groups, fr_LP_results, *len_align, ia, ja, ar, &row, &mat_i);
    if(nb_cycles < 0)
    {
      printf("\n!!! ERROR: Cannot produce LP results !!!\n\n\n");
      CPXfreeprob(env, &lp);
      CPXcloseCPLEX(&env);
      return(0);
    }
    else if(nb_cycles > 0)
      round_no++;
  }




  if(running_time)
  {
    elapsed = difftime(end_time, real_start_time);
    elapsed_hour = elapsed / 3600;
    elapsed_min = (elapsed - (elapsed_hour * 3600)) / 60;
    elapsed_sec = (elapsed - (elapsed_hour * 3600) - (elapsed_min * 60));
    printf("****************************************************\n");
    printf("Solution found!\n");
    printf("%i Round(s) needed\n", round_no);
    printf("Total time: %i hour(s), %02i minute(s) and %02i second(s)\n",
	   elapsed_hour, elapsed_min, elapsed_sec);
    printf("****************************************************\n\n");
  }
  else if(verbose)
  {
    printf("*****************\n");
    printf("Solution found!\n");
    printf("%i Round(s) needed\n", round_no);
    printf("*****************\n\n");
  }
  
  // clean up
  CPXfreeprob(env, &lp);
  CPXcloseCPLEX(&env);
  free(ia);
  free(ja);
  free(ar);
  
  return(1);
}


// **************
// Return -true- if the two substrings (a[i]-a[i+len] and a[x]-a[x+len])
// are identical.
int substring_EQ(int len_T, struct ARNt_CODE T[len_T], int i, int x, int len)
{
  while(len--)
    if(T[i+len].id != T[x+len].id)
      return 0;

  return 1;
}


// **************
// Take a genome and return the longest duplication.
// (non-overlapping, identical substrings)
// ** we do this the slow and obvious way (O(n^4)).
int get_longest_duplication(int len_T, struct ARNt_CODE T[len_T])
{
  int i, j, x, len;
  int longest = 0;  // The longest dup found so far.

  for(i=0; i < (len_T - 1); i++)
  {  
    for(j=i; j < len_T; j++)                                       // For each substring
    {
      len = j-i+1;
      for(x = j+1; (x+len-1) < len_T; x++)                           // and a substring to the right of it
      { 
	if((len > longest) && substring_EQ(len_T, T, i, x, len))     // if they are a long match
	  longest = len;                                             // store the length of the match.
      }
    }
  }
  return longest;
}


// **************
void encode_classic(int len_X, struct ARNt_CODE X[len_X], int len_Y, struct ARNt_CODE Y[len_Y], char *X_temp, char *Y_temp)
{
  int i, j;
  int id;
  unsigned char found;

  id = 1;

  // encode 'X'
  for(i=0; i<len_X; i++)
  {
    found = 0;
    for(j=0; j<i && !found; j++)
    {
      if(X_temp[i] == X[j].code[0])
      {
	X[i].id = X[j].id;
	found = 1;
      }
    }
    if(!found)
    {
      X[i].id = id;
      id++;
    }
    sprintf(X[i].code, "%c", X_temp[i]);
    X[i].print_no = i + 1;
  }

  // encode 'Y'
  for(i=0; i<len_Y; i++)
  {
    found = 0;
    for(j=0; j<len_X && !found; j++)
    {
      if(Y_temp[i] == X[j].code[0])
      {
	Y[i].id = X[j].id;
	found = 1;
      }
    }
    if(!found)
    {
      for(j=0; j<i && !found; j++)
      {
	if(Y_temp[i] == Y[j].code[0])
	{
	  Y[i].id = Y[j].id;
	  found = 1;
	}
      }
      if(!found)
      {
	Y[i].id = id;
	id++;
      }
    }
    sprintf(Y[i].code, "%c", Y_temp[i]);
    Y[i].print_no = i + 1 + len_X;
  }
}


// **************
void encode_quads(int len_X, struct ARNt_CODE X[len_X], int len_Y, struct ARNt_CODE Y[len_Y], char *X_temp, char *Y_temp)
{
  int i, j;
  int id;
  unsigned char found;
  char *code;

  id = 1;

  // encode 'X'
  i = 0;
  code = strtok(X_temp,",;");
  while(code != NULL)
  {
    strcpy(X[i].code, code);
    X[i].print_no = i + 1;

    found = 0;
    for(j=0; j<i && !found; j++)
    {
      if(!strcmp(X[i].code, X[j].code))
      {
	X[i].id = X[j].id;
	found = 1;
      }
    }
    if(!found)
    {
      X[i].id = id;
      id++;
    }

    i++;
    code = strtok(NULL,",;");
  }
  
  // encode 'Y'
  i = 0;
  code = strtok(Y_temp,",;");
  while(code != NULL)
  {
    strcpy(Y[i].code, code);
    Y[i].print_no = i + 1 + len_X;

    found = 0;
    for(j=0; j<len_X && !found; j++)
    {
      if(!strcmp(Y[i].code, X[j].code))
      {
	Y[i].id = X[j].id;
	found = 1;
      }
    }
    if(!found)
    {
      for(j=0; j<i; j++)
      {
	if(!strcmp(Y[i].code, Y[j].code))
	{
	  Y[i].id = Y[j].id;
	  found = 1;
	}
      }
      if(!found)
      {
	Y[i].id = id;
	id++;
      }
    }
    i++;
    code = strtok(NULL,",;");
  }
}


// **************
int count_strings(char *T)
{
  int i;
  char *code;
  char *T_temp;

  T_temp = (char*)malloc(strlen(T) + 1);
  strcpy(T_temp, T);
  i = 0;

  code = strtok(T_temp,",;");
  while(code != NULL)
  {
    i++;
    code = strtok(NULL,",;");
  }
  
  free(T_temp);
  return(i);
}


// **************
unsigned char read_genome_file(char fname[100], char **genome)
{
  FILE *fp;
  long start, nbchar;
  char c;
  unsigned char comments;

  // open file
  fp = fopen(fname, "r");
  if(fp == NULL)
  {
    printf("Error: cannot open file '%s'\n",fname ) ;
    return(1);
  }

  // ignore comments
  comments = 1;
  while(comments)
  {
    c = fgetc(fp);
    if(c == '#')
    {
      while(c!='\n')
	c = fgetc(fp);
    }
    else
    {
      comments = 0;
    }
  }
  start = ftell(fp) - 1;
  
  fseek(fp,0,SEEK_END);
  nbchar = ftell(fp) - start;
  *genome = (char*)malloc(nbchar + 1);   // +1 for the '\0'
  
  fseek(fp,start,SEEK_SET);
  fscanf(fp, "%s", *genome);
  
  fclose(fp);
  return(0);
}


// **************
void print_usage()
{
  printf("\n");
  printf("Usage: ./LP_align_CPLEX [-fqdtpvh] genome1 genome2\n");
  printf("Option: -f : genomes from filenames (otherwise it's from console)\n");
  printf("        -q : ARNt quad format (delimited by a ',' or ';')\n");
  printf("        -d : show the detailed results\n");
  printf("        -t : show running time\n");
  printf("        -p : show the 3 dot plots\n");
  printf("        -v : verbose\n");
  printf("        -h : show the usage help (this message!)\n");
  printf("\n");
  printf("Example: ./LP_align_CPLEX -dtpv abcab c\n");
  printf("\n");
}


// **************
int check_params(int argc, const char *argv[], char **X_temp, char **Y_temp, unsigned char *show_dotplot, 
		 unsigned char *detailed_results, unsigned char *running_time)
{
  int i, j;
  char option;
  short param_no = 0;
  unsigned char error_reading_file = 0;
  char fname1[100], fname2[100];
  unsigned char data_in_files = 0;

  if(argc < 3)
  {
    print_usage();
    return(0);
  }
  
  for(i=1; i<argc; i++)
  {
    if(argv[i][0] == '-')
    {
      for(j=1; j<strlen(argv[i]); j++)
      {
	option = tolower(argv[i][j]);
	switch(option)
	{ 
	  case 'p':
	  {
	    *show_dotplot = 1;
	    break;
	  }
	  
	  case 'f':
	  {
	    data_in_files = 1;
	    break;
	  }
	  
	  case 'v':
	  {
	    verbose = 1;
	    break;
	  }
	  
	  case 'd':
	  {
	    *detailed_results = 1;
	    break;
	  }
	  
	  case 't':
	  {
	    *running_time = 1;
	    break;
	  }
	  
	  case 'q':
	  {
	    ARNt_quad_format = 1;
	    break;
	  }

	  case 'h':
	  {
	    print_usage();
	    return(0);
	  }

	  default:
	  {
	    printf("Unknown Option: '%c'\n", option);
	    return(0);
	  }
	}
      }
    }
  }
  
  for(i=1; i<argc; i++)
  {
    if(argv[i][0] != '-')
    {
      if(!data_in_files)  // console
      {
	if(param_no == 0)
	{
	  *X_temp = (char*)malloc(strlen(argv[i]) + 1);
	  strcpy(*X_temp, argv[i]);
	}
	else
	{
	  if(param_no == 1)
	  {
	    *Y_temp = (char*)malloc(strlen(argv[i]) + 1);
	    strcpy(*Y_temp, argv[i]);
	  }
	}
      }
      else  // in files
      {
	if(param_no == 0)
	{
	  strcpy(fname1, argv[i]);
	  error_reading_file = read_genome_file(fname1, X_temp);
	}
	else
	{
	  if(param_no == 1)
	  {
	    strcpy(fname2, argv[i]);
	    error_reading_file = error_reading_file + read_genome_file(fname2, Y_temp);
	  }
	}
      }
      param_no++;
    }
  }

  if(error_reading_file)
  {
    free(*X_temp);
    free(*Y_temp);
    return(0);
  }
  
  if(param_no != 2)
  {
    printf("Error on command line.\n");
    print_usage();
    free(*X_temp);
    free(*Y_temp);
    return(0);
  }
  
  return(1);
}


// ***************************************************************
// ***************************************************************
int main(int argc, const char* argv[])
{
  unsigned char show_dotplot = 0;
  unsigned char detailed_results = 0;
  unsigned char running_time = 0;
  char *X_temp=0, *Y_temp=0;
  int len_X, len_Y;
  int dotplot_X_nb_groups, dotplot_Y_nb_groups, dotplot_Z_nb_groups;
  int longest_dup_X, longest_dup_Y, max_groups_per_elem_X, max_groups_per_elem_Y;
  int i, j;

  
  // **********************************************
  // ***** SECTION I: READ PARAMETERS & FILES *****
  // **********************************************
  // check parameters
  if(!check_params(argc, argv, &X_temp, &Y_temp, &show_dotplot, &detailed_results, &running_time))
    return(0);

  // encode the strings with an id
  if(!ARNt_quad_format)
  {
    len_X = strlen(X_temp);
    len_Y = strlen(Y_temp);
  }
  else
  {
    len_X = count_strings(X_temp);
    len_Y = count_strings(Y_temp);
  }

  struct ARNt_CODE X[len_X];
  struct ARNt_CODE Y[len_Y];

  if(!ARNt_quad_format)
  {
    encode_classic(len_X, X, len_Y, Y, X_temp, Y_temp);
  }
  else
  {
    encode_quads(len_X, X, len_Y, Y, X_temp, Y_temp);
  }

  free(X_temp);
  free(Y_temp);


  // **********************************************
  // ***** SECTION II: DOT PLOT *******************
  // **********************************************
  longest_dup_X = get_longest_duplication(len_X, X);
  longest_dup_Y = get_longest_duplication(len_Y, Y);
  if(verbose)
  {
    printf("Longest possible duplications...\n");
    printf("...for X =  %i\n", longest_dup_X);
    printf("...for Y =  %i\n\n", longest_dup_Y);
  }
  max_groups_per_elem_X = (int)(((longest_dup_X + 1) * longest_dup_X) / 2.0 + 0.5) + 1;
  max_groups_per_elem_Y = (int)(((longest_dup_Y + 1) * longest_dup_Y) / 2.0 + 0.5) + 1;

  int ***dotplot_X;
  dotplot_X = (int***)malloc(len_X * sizeof(int**));
  for(i=0; i<len_X; i++)
  {
    dotplot_X[i] = (int**)malloc(len_X * sizeof(int*));
    for(j=0; j<len_X; j++)
      dotplot_X[i][j] = (int*)malloc(max_groups_per_elem_X * sizeof(int));
  }

  int ***dotplot_Y;
  dotplot_Y = (int***)malloc(len_Y * sizeof(int**));
  for(i=0; i<len_Y; i++)
  {
    dotplot_Y[i] = (int**)malloc(len_Y * sizeof(int*));
    for(j=0; j<len_Y; j++)
      dotplot_Y[i][j] = (int*)malloc(max_groups_per_elem_Y * sizeof(int));
  }

  int **dotplot_Z;
  dotplot_Z = (int**)malloc(len_X * sizeof(int*));
  for(i=0; i<len_X; i++)
    dotplot_Z[i] = (int*)malloc(len_Y * sizeof(int));
  
  create_dup_dotplot(len_X, X, max_groups_per_elem_X, dotplot_X, &dotplot_X_nb_groups);
  create_dup_dotplot(len_Y, Y, max_groups_per_elem_Y, dotplot_Y, &dotplot_Y_nb_groups);
  create_match_dotplot(len_X, X, len_Y, Y, dotplot_Z, &dotplot_Z_nb_groups);

  if(show_dotplot)
  {
    print_dup_dotplot(len_X, X, len_X, X, dotplot_X);
    print_dup_dotplot(len_Y, Y, len_Y, Y, dotplot_Y);
    print_match_dotplot(len_X, X, len_Y, Y, dotplot_Z);
    if(ARNt_quad_format)
      show_keys(len_X, X, len_Y, Y);
  }
  
  
  // **********************************************
  // ***** SECTION III: LINEAR PROGRAMMING ********
  // **********************************************
  int size_LP_results = 1 + len_X + len_Y + dotplot_X_nb_groups + dotplot_Y_nb_groups + dotplot_Z_nb_groups;
  int LP_results[size_LP_results];
  struct FRIENDLY_LP_RESULTS_STRUCT friendly_LP_results[len_X + len_Y];
  struct ALIGN_CODE align_X[len_X + len_Y];
  struct ALIGN_CODE align_Y[len_X + len_Y];
  struct ALIGN_CODE align_ancestor[len_X + len_Y];
  int len_align;
  
  if(resolve_LP(len_X, X, len_Y, Y, 
		max_groups_per_elem_X, max_groups_per_elem_Y,
		dotplot_X, dotplot_X_nb_groups, dotplot_Y, dotplot_Y_nb_groups,
		dotplot_Z, dotplot_Z_nb_groups, size_LP_results, LP_results, running_time,
		friendly_LP_results, align_X, align_Y, &len_align))
  {	     
  
    // **********************************************
    // ***** SECTION IV: RESULTS ********************
    // **********************************************
    if(detailed_results)
      print_detailed_results(len_X + len_Y, friendly_LP_results, LP_results[0], len_align);

    find_ancestor(len_X + len_Y, friendly_LP_results, align_ancestor, len_align);
    if(!ARNt_quad_format)
      print_summary_results_classic(len_X + len_Y, align_X, align_Y, align_ancestor, len_align);
    else
      print_summary_results_quads(len_X + len_Y, align_X, align_Y, align_ancestor, len_align, detailed_results);
  }


  // **********************************************
  // ***** SECTION V: CLEAN UP & DONE *************
  // ********************************************** 
  for(i=0; i<len_X; i++)
  {
    for(j=0; j<len_X; j++)
    {
      free(dotplot_X[i][j]);
    }
    free(dotplot_X[i]);
  }
  free(dotplot_X);

  for(i=0; i<len_Y; i++)
  {
    for(j=0; j<len_Y; j++)
    {
      free(dotplot_Y[i][j]);
    }
    free(dotplot_Y[i]);
  }
  free(dotplot_Y);

  for(i=0; i<len_X; i++)
    free(dotplot_Z[i]);
  free(dotplot_Z);

  printf("Done.\n");
  return(0);
}

