from typing import Tuple, List

ORIGIN = '[o]'
TERMINUS = '[t]'
CIRCULAR = ')'
LINEAR = '|'

GETDIST = 'bin/getdist'    #: Binary for estimating a distance between strings


#TYPES:

MATCH = Tuple[int, int]               #: Single gene match between chrom
INTERVAL = Tuple[int, int]            #: Interval (python indexed) of a chrom
INTMATCH = Tuple[INTERVAL, INTERVAL]  #: Pair of matching intervals
CHROMMATCHES = Tuple[int, int, List[INTMATCH]]  #: matches between chroms
