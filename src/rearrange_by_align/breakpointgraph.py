"""
The BreakpointGraph is the fundamental tool used for genome comparison.
"""

import copy

from collections import defaultdict, Counter
from typing import Dict, FrozenSet, List, Set, Tuple
import networkx as nx
from itertools import chain, combinations, product, zip_longest
from itertools import tee

from rearrange_by_align import ORIGIN

from .genome import Chromosome, Genome
from .genome import unsigned

# Edge colors (use strings when printing the graph to a dot file):
COLOR = bool
BLACK = True
GRAY = False

TAIL = '_t'
HEAD = '_h'
TELOMERE = '_O_'

# Type definitions:
MEDGE = Tuple[str, str, int]      #A multigraph edge is (vertex, vertex, key)
PATH = List[MEDGE]
TRIO = Tuple[str, str, str]       #A length-3 path

#_______________________________________________________________________________


class ColoredPath:
  """
  A colored path of edges that knows its monochromatic vertices (i.e. the
  vertices that are incident to two edges of the same color). This is used
  to represent the cycles in the breakpoint graph.

  Notes
  -----
  The color of edge `self.path[i]` can be accessed using the nx graph by
  doing `G.edges[*self.path[i]]`.

  Attributes
  ----------
  path : List[MEDGE]
      the list of edges (u, v, key) that make this colored cycle (the key is
      needed since we are dealing with multi-graphs)
  mvTOcolor : Dict[str, COLOR]
      map monochromatic vertex to color. A monochromatic vertex is one that
      is adjacent to two edges of the same color.
  firstcolor : COLOR
      the color of the first edge in the path
  orderededges : Set[Tuple(str, str)]
      the edges of `self.path` where each edge is a tuple (u,v,k) where u and
      v are in alphabetical order. set lazily when needed
  vertexset : Dict[str, int]
      map the vertices of the path to the index in `self.path` such that
      `self.path[self.vertexset[v]][0] == v`. set lazily when needed
  """
  def __init__(self, cp: 'ColoredPath'=None,
               inittrio: Tuple[List[MEDGE], Dict[str, COLOR], COLOR]=None):
    if cp:
      self.path: List[MEDGE] = list(cp.path)
      self.mvTOcolor: Dict[str, COLOR] = dict(cp.mvTOcolor)
      self.firstcolor: COLOR = cp.firstcolor
    elif inittrio:
      self.path = inittrio[0]
      self.mvTOcolor = inittrio[1]
      self.firstcolor = inittrio[2]
    else:
      self.path = []
      self.mvTOcolor = {}
      self.firstcolor = None  # type: ignore
    
    self.orderededges: Set[Tuple[str, str, int]] = set()        #Lazily set.
    self.vertexTOpos: Dict[str, List[int]] = defaultdict(list)  #Lazily set.

  def append(self, u:str, v:str, key:int):
    self.path.append((u, v, key))

  def pop(self) -> MEDGE:
    return self.path.pop()

  def addMonoVertex(self, v: str, color: COLOR):
    self.mvTOcolor[v] = color

  def removeMonoVertex(self, v: str):
    del self.mvTOcolor[v]

  def setFirstColor(self, c: COLOR):
    self.firstcolor = c

  def monochromaticVertices(self):
    return self.mvTOcolor.keys()

  def isBalanced(self):
    return not self.mvTOcolor

  def compatibleWith(self, other: 'ColoredPath') -> str:
    """
    Two cycles are compatible iff they share exactly one monochromatic vertex
    and the color of the incident edges are opposite in each cycle.

    Note
    ----
        Also check that there are no other shared vertices, since this would
        imply non-simplicy of the cycle (there are two alternating cycles that
        could be made).

    Parameters
    ----------
    other : ColoredPath
        the other cycle

    Returns
    -------
    str
        if the two are compatible, return the shared vertex
    """
    shared = self.sharedVertices(other)
    if len(shared) == 1:
      v = shared.pop()
      if(v in self.mvTOcolor and v in other.mvTOcolor and
         self.mvTOcolor[v] != other.mvTOcolor[v]):
        return v

    return ''

  def sharedVertices(self, other: 'ColoredPath') -> Set[str]:
    """
    Return the set of shared vertices between these two ColoredPaths.

    Parameters
    ----------
    other : ColoredPath
        the other cycle

    Returns
    -------
    Set[str]
        the vertices in both cycles
    """
    if not self.vertexTOpos:
      self._setVertices()
    if not other.vertexTOpos:
      other._setVertices()

    return  self.vertexTOpos.keys() & other.vertexTOpos.keys()

  def sharedEdges(self, other: 'ColoredPath') -> Set[Tuple[str, str, int]]:
    if not self.orderededges:
      self._setOrderedEdges()
    if not other.orderededges:
      other._setOrderedEdges()

    return  self.orderededges & other.orderededges

  def containsTrio(self, a: str, b: str, c: str) -> bool:
    """
    Return true if the path a, b, c is a subpath of this path.

    Parameters
    ----------
    a : str
        first vertex
    b : str
        second vertex
    c : str
        third vertex

    Returns
    -------
    bool
        True if a-b-c is a subpath
    """
    if not self.vertexTOpos:
      self._setVertices()

    for pos in self.vertexTOpos[b]:
      if((a == self.path[pos-1][0] and c == self.path[pos][1]) or
         (c == self.path[pos-1][0] and a == self.path[pos][1])):
        return True

    return False

  def _setOrderedEdges(self):
    self.orderededges = set((v,u,k) if u > v else (u,v,k) for u,v,k in self.path)

  def _setVertices(self):
    for i, (u,_,_) in enumerate(self.path):
      self.vertexTOpos[u].append(i)

  def __len__(self):
    return len(self.path)

  def __getitem__(self, key):
    return self.path[key]

  def __iter__(self):
    return iter(self.path)

  def __repr__(self):
    return str(self)

  def __str__(self):
    """
    This prints all the vertices of the path.
    """
    if self.path[0][0] == self.path[-1][1]:   # a cycle
      return str([u for u, _, _ in self.path])
    else:
      return str([u for u, _, _ in self.path] + [self.path[-1][1]])


  def __contains__(self, vertex: str) -> bool:
    if not self.vertexTOpos:
      self._setVertices()

    return vertex in self.vertexTOpos

#_______________________________________________________________________________

class BreakpointGraph:
  """
  A BreakpointGraph for two circular genomes. It knows how to enumerate its
  simple alternating cycles. There can be many of these in the presence of
  duplicated genes.

  Attributes
  ----------
  g1 : Genome
      first genome
  g2 : Genome
      second genome
  G : MultiGraph
      the breakpoint graph
  extremities1: List[Tuple[str, str]]
      the list of pairs of extremities in the order of genome1
  extremities2: List[Tuple[str, str]]
      the list of pairs of extremities in the order of genome2
  geneTOcount1: Dict[str, int]
      map gene to number of occurences in genome1
  geneTOcount2: Dict[str, int]
      map gene to number of occurences in genome2
  """

  def __init__(self, genome1: Genome, genome2: Genome):
    """
    Build the BreakpointGraph which is accessible through `self.G`.

    Parameters
    ----------
    genome1 : Genome
        [description]
    genome2 : Genome
        [description]
    """
    self.g1 = genome1
    self.g2 = genome2

      #Add the regular edges:
    self.G = nx.MultiGraph()
    self.extremities1: List[Tuple[str, str]] = []
    for chrom in genome1:
      self.extremities1 += self._addEdgesForChromosome(chrom, BLACK)

    self.extremities2: List[Tuple[str, str]] = []
    for chrom in genome2:
      self.extremities2 += self._addEdgesForChromosome(chrom, GRAY)

      #Add the "ghost" edges to emulate indels:
    self.geneTOcount1: Dict[str, int] = Counter()
    for chromosome in genome1:
      for gene in map(unsigned, chromosome):
        self.geneTOcount1[gene] += 1
    self.geneTOcount2: Dict[str, int] = Counter()
    for chromosome in genome2:
      for gene in map(unsigned, chromosome):
        self.geneTOcount2[gene] += 1

    for gene in set(self.geneTOcount1).union(self.geneTOcount2):
      diff = self.geneTOcount1[gene] - self.geneTOcount2[gene]
      if diff > 0:                              #Excess in genome 1.
        self.G.add_edges_from([extremities(gene)]*diff, color=GRAY,
                              marked=False)
      elif diff < 0:                            #Excess in genome 1.
        self.G.add_edges_from([extremities(gene)]*abs(diff), color=BLACK,
                              marked=False)

      #Add a self loop for each unbalanced cycle:
    _, ub_black, ub_gray = self.getBasicCycles()
    i = 0
    for i in range(len(ub_black)):
      self.G.add_edge(TELOMERE, TELOMERE, i, color=GRAY)
    for j in range(i+1, len(ub_gray)+i+1):
      self.G.add_edge(TELOMERE, TELOMERE, j, color=BLACK)


  def _addEdgesForChromosome(self, chrom: Chromosome, color: COLOR,
                            ) -> List[Tuple[str, str]]:
    """
    Add the edges to the graph corresponding to adjacencies in `chrom`.

    Parameters
    ----------
    chrom : Chromosome
        the chromosome
    color : COLOR
        the color of the genome

    Returns
    -------
    List[Tuple[str, str]]
        the extremities for the chromosome
    """
    extlist = list(map(extremities, chrom))

    self.G.add_nodes_from(chain.from_iterable(extlist))

      #Add edges:
    edges = []
    for (_, left), (right, _) in pairwise(extlist):
      edges.append((left, right))
    self.G.add_edges_from(edges, color=color, marked=False)
    if chrom.linear:
      self.G.add_edge(TELOMERE, extlist[0][0], color=color, marked=False)
      self.G.add_edge(extlist[-1][1], TELOMERE, color=color, marked=False)
    else:
      self.G.add_edge(extlist[-1][1], extlist[0][0], color=color, marked=False)

    return extlist

  #   -   -   -   -   -   -   -   Cycles   -   -   -   -   -   -   -   -   -
  def getBasicCycles(self) -> Tuple[List[ColoredPath], List[ColoredPath], List[ColoredPath]]:
    """
    Classify the cycles of this graph into balanced (equal number of black and
    gray edges) and unbalanced (an extra black or gray).

    NOTES:
    -----
        Assumes that there are no duplicate genes!

    Returns
    -------
    Tuple[List[ColoredPath], List[ColoredPath], List[ColoredPath]]
        The tuple [balanced, unbalanced_black, unbalanced_gray] where
        unbalanced_black (unbalanced_gray) are the cycles that touch the
        telomere vertex and have one extra black (gray) edge.
    """
    assert not self.hasDuplicates()

    visited: Set[str] = set()       #Vertices that have been visited.
    balanced: List[ColoredPath] = []
    ub_gray: List[ColoredPath] = []
    ub_black: List[ColoredPath] = []

      #Visit all cycles that stem from the telomere vertex:
    if TELOMERE in self.G:
      visited.add(TELOMERE)

      for v in self.G[TELOMERE]:
        if v in visited:
          continue                # v is already in a cycle

        cycle = self._visitCycle(TELOMERE, v, visited)

        if self.G.edges[cycle.path[0]]['color'] == self.G.edges[cycle.path[-1]]['color']:
          if self.G.edges[cycle.path[0]]['color'] == GRAY:
            ub_gray.append(cycle)
          else:
            ub_black.append(cycle)
        else:
          balanced.append(cycle)

        #Visit self loops:
      if TELOMERE in self.G[TELOMERE]:
        for i in self.G[TELOMERE][TELOMERE]:
          if self.G[TELOMERE][TELOMERE][i]['color'] == BLACK:
            ub_black.append(ColoredPath(inittrio=([(TELOMERE, TELOMERE, i)],
                                                  {TELOMERE: BLACK}, BLACK)))
          else:
            ub_gray.append(ColoredPath(inittrio=([(TELOMERE, TELOMERE, i)],
                                                 {TELOMERE: GRAY}, GRAY)))

      #Get other cycles:
    for v in self.G:
      if v in visited:
        continue

      cycle = self._visitCycle(v, list(self.G[v])[0], visited)
      balanced.append(cycle)

    return balanced, ub_black, ub_gray

  def _visitCycle(self, seed1: str, seed2, visited: Set[str]) -> ColoredPath:
    """
    Build a cycle from the seed edge, assuming there are no forks in the path.

    Parameters
    ----------
    seed1 : str
        the first seed vertex
    seed2 : str
        the second seed vertex
    visited : Set[str]
        the set of visited vertices

    Returns
    -------
    ColoredPath
        the cycle
    """
    cycle = ColoredPath()

    visited.add(seed1)
    v = seed2
    if len(self.G[v]) == 1:   # Cycle _O_ - v - _O_
      cycle.append(seed1, v, 0)
      cycle.append(v, seed1, 1)
      visited.add(v)
    else:
      u = seed1
      w = v
      while v == w:
        cycle.append(u, v, 0) # Guaranteed a single edge since no duplicates.
        visited.add(v)
        u = v
        for w in self.G[u]:
          if w != seed1 and w not in visited:
            v = w
            break

      cycle.append(u, seed1, 0)

    return cycle
            

  def hasDuplicates(self) -> bool:
    """
    Return True if either genome has duplicate genes.
    """
    return(any(c > 1 for c in self.geneTOcount1.values()) and
           any(c > 1 for c in self.geneTOcount2.values()))

  def getSimpleAlternatingCycles(self) -> List[ColoredPath]:
    """
    Return a list of all alternating cycles in this graph (those cycles that
    could be part of a maximum cycle decomposition).

    Returns
    -------
    List[ColoredPath]
        the list of cycles
    """
    simpleversion = nx.Graph(self.G)
    coloredcycles = self._addColors(getSimpleCycles(simpleversion))

      #Partition cycles into alternating (can't combine with others), and others:
    alternatingcycles: List[ColoredPath] = []
    others: List[ColoredPath] = []
    for cycle in coloredcycles:
      if cycle.monochromaticVertices():
        others.append(cycle)
      else:
        alternatingcycles.append(cycle)

      #Add length-2 cycles to the others:
    len2mono, len2alternating = self._getLength2Cycles(simpleversion)
    alternatingcycles += len2alternating
    others += len2mono

    #print('ALTERNATING:')
    #for c in alternatingcycles:
    #  print(f'\t{c}')
    #print('OTHERS:')
    #for c in others:
    #  print(f'\t{c} {c.mvTOcolor}')

      #Build the compatability graph:
    if others:
      edges = []              #Edges in the compatiblity graph between cycles
      for c1, c2 in combinations(others, 2):
        if sharedv := c1.compatibleWith(c2):
          edges.append((c1, c2, {'monov': sharedv}))

      cg = nx.Graph()         #Compatibility graph between cycles
      cg.add_nodes_from(others, visited=False)
      cg.add_edges_from(edges)

        #Look for complete trees in the compatability graph:
      for v in cg:                    #Associate to each node a way to reference
        mvTOneigh = defaultdict(set)  #cg edges by monochromatic vertex
        for u in cg[v]:
          mvTOneigh[cg.edges[v, u]['monov']].add(u)
        cg.nodes[v]['mvTOneigh'] = mvTOneigh

      completetrees = []
      for v in cg:                  #Now build the complete trees...
        if len(v.mvTOcolor) == 1:   #For each potential leaf,
          completetrees += getCompleteTrees(cg, v)  #try to build complete trees

      cycles = self._subtreesToCycles(completetrees)
      #print(f'composite cycles:')
      #for cycle in cycles:
      #  print(f'\t{cycle}')

      alternatingcycles += cycles

    return alternatingcycles

  def _subtreesToCycles(self, subtrees: List[FrozenSet[ColoredPath]],
                        onepercycle=False) -> List[ColoredPath]:
    """
    Combine each set of cycles (that together form a complete subtree) into all
    the posssible alternating colored cycle that they can combine to form.

    Parameters
    ----------
    subtrees : List[FrozenSet[ColoredPath]]
        list of complete subtrees of the compatibility graph
    onepercycle : bool
        simple alternating cycles that revisit the same vertex will have
        multiple ways to order the edges, only return a single ordering if
        this is True

    Returns
    -------
    List[ColoredPath]
        list of cycles that are combinations of the cycles forming each subtree
    """
    retlist = []
    for subtree in set(subtrees):   #explore unique subtrees
      self._markCycle(subtree)

      if onepercycle:
        retlist.append(self._getMarkedCycle(next(iter(subtree)).path[0]))
      else:
        retlist += self._getMarkedCycles(next(iter(subtree)).path[0])
        self._markCycle(subtree, False)

      assert self._assertUnmarked()

    return retlist

  def _getMarkedCycles(self, edge: MEDGE) -> List[ColoredPath]:
    """
    Return the alternating cycles that starts at the given edge and follow
    edges that have been marked by `_markCycle()`. There is more than one
    cycle due to the potential of having four marked edges from a vertex; we
    can choose either way to follow. The graph _will_ remain marked.

    Parameters
    ----------
    edge : MEDGE
        the edge to start the cycle traversal at

    Returns
    -------
    ColoredPath
        the cycle that was marked
    """
    u, v, k = edge
    prevcolor = self.G.edges[u, v, k]['color']
    self.G.edges[u, v, k]['marked'] = False

    cyclelist: List[ColoredPath] = []
    path = ColoredPath()
    path.append(u, v, k)
    self._extendPath(cyclelist, path, v, prevcolor)

    return cyclelist

  def _extendPath(self, cyclelist: List[ColoredPath], pathsofar: ColoredPath,
                  u: str, prevcolor: COLOR):
    """
    Recursively extend the `pathsofar` through all edges with a color opposite
    to `prevcolor`. Stop when we run out of marked edges to follow.

    Parameters
    ----------
    cyclelist : List[ColoredPath]
        the list of cycles we've visited
    pathsofar : ColoredPath
        the path we've build so far
    u : str
        the curret vertex
    prevcolor : COLOR
        the color of the last edge we explored
    """
    incident = self._getMarkedWithColor(u, not prevcolor)
    if not incident:
      cyclelist.append(ColoredPath(pathsofar))
    else:
      for u, v, k in incident:
        pathsofar.append(u, v, k)
        self.G.edges[u, v, k]['marked'] = False
        self._extendPath(cyclelist, pathsofar, v, not prevcolor)
        self.G.edges[u, v, k]['marked'] = True
        pathsofar.pop()

    

  def _getMarkedWithColor(self, u: str, color: COLOR) -> List[MEDGE]:
    """
    Get edges incident to this vertex with the given color.

    Parameters
    ----------
    u : str
        the vertex
    color : COLOR
        the color

    Returns
    -------
    List[MEDGE]
        list of incident edges with given color
    """
    retlist = []
    for v in self.G[u]:
      for k in self.G[u][v]:
        if(self.G.edges[u, v, k]['marked'] and
           self.G.edges[u, v, k]['color'] == color):
          retlist.append((u, v, k))

    return retlist

  def _getMarkedCycle(self, edge: MEDGE) -> ColoredPath:
    """
    Return an alternating cycle that starts at the given edge. Assume that the
    edges of the path have been marked already by `_markCycle()`. Leave the
    graph unmarked.

    Parameters
    ----------
    edge : MEDGE
        the edge to start the cycle traversal at

    Returns
    -------
    ColoredPath
        the cycle that was marked
    """
    first, u, k = edge
    prevcolor = self.G.edges[first, u, k]['color']
    self.G.edges[first, u, k]['marked'] = False
    path = ColoredPath()
    path.append(*edge)
    while u != first or self._hasMarkedEdge(u):
      for v in self.G[u]:
        found = False
        for k in self.G[u][v]:
          if(self.G.edges[u, v, k]['marked'] and
             self.G.edges[u, v, k]['color'] != prevcolor):
            path.append(u, v, k)
            self.G.edges[u, v, k]['marked'] = False
            u = v
            prevcolor = not prevcolor
            found = True
            break

        if found:
          break

    return path

  def _assertUnmarked(self, output=True):
    """
    Return False if any of the edges are marked as True.
    """
    for n, nbrsdict in self.G.adjacency():
      for nbr, keydict in nbrsdict.items():
          for key, eattr in keydict.items():
            if eattr['marked']:
              if output:
                print(f' !!{n, nbr, key} is marked!!')
              return False

    return True

  def _hasMarkedEdge(self, u: str) -> bool:
    """
    Return True if there is a neighbor of u that is marked.

    Parameters
    ----------
    u : str
        the vertex to check
    """
    for v in self.G[u]:
      for k in self.G[u][v]:
        if self.G.edges[u, v, k]['marked']:
          return True

    return False

  def _markCycle(self, cycle: FrozenSet[ColoredPath], markwith=True):
    """
    Mark the edges of the given cycle with the attribute 'marked' equal to the
    value of `markwith`.

    Parameters
    ----------
    cycle : Set[ColoredPath]
        the cycle as a set of ColoredPaths
    markwith : bool, optional
        mark with this value, by default True
    """
    for c in cycle:
      for u, v, k in c:
        self.G.edges[u, v, k]['marked'] = markwith


  def _getLength2Cycles(self, simpleG: nx.Graph
                        ) -> Tuple[List[ColoredPath], List[ColoredPath]]:
    """
    Return a list of length-2 monochromatic and polychromatic cycles.

    Parameters
    ----------
    simpleG : nx.Graph
        the simple version of `self.G`

    Returns
    -------
    Tuple[List[ColoredPath], List[ColoredPath]]
        the monochromatic and polychromatic length-2 cycles (mono, poly)
    """
    mono = []
    poly = []
    visited = set()
    for u, v in simpleG.edges:        
      edge = sortedPair(u, v)
      if edge not in visited:
        visited.add(edge)
        black = []
        gray = []
        for key in self.G[u][v]:
          if self.G[u][v][key]['color'] == BLACK:
            black.append((u, v, key))
          else:
            gray.append((u, v, key))

        for e1, e2 in combinations(black, 2):
          mono.append(ColoredPath(inittrio=([e1, (e2[1], e2[0], e2[2])],
                                            {u: BLACK, v: BLACK}, BLACK)))
        for e1, e2 in combinations(gray, 2):
          mono.append(ColoredPath(inittrio=([e1, (e2[1], e2[0], e2[2])],
                                            {u: GRAY, v: GRAY}, GRAY)))
        for e1, e2 in product(gray, black):
          poly.append(ColoredPath(inittrio=([e1, (e2[1], e2[0], e2[2])],
                                            {}, GRAY)))

    return mono, poly

  def _addColors(self, cycles: List[List[str]]) -> List[ColoredPath]:
    """
    Given cycles in the simple uncolored version of `self.G`, return a list
    of colored versions of the cycles. Since `self.G` is a multigraph, there
    could be many colored cycles per uncolored cycle.

    Parameters
    ----------
    cycles : List[List[str]]
        the uncolored cycles

    Returns
    -------
    List[ColoredPath]
        list of colored cycles, each knowing its set of monochromatic vertices
    """
    retval = []
    for c in cycles:
      retval.extend(self._getColored(c))
    return retval

  def _getColored(self, cycle: List[str]) -> List[ColoredPath]:
    """
    Return a list of ColoredPath, each knowing the path of edges taken, along
    with the monochromatic vertices.

    Parameters
    ----------
    cycle : List[str]
        the uncolored cycle to inspect

    Returns
    -------
    List[ColoredPath]
        the list of ColoredPaths corresponding to `cycle`
    """
    retvalues: List[ColoredPath] = []
    self._recColored(retvalues, cycle)
    return retvalues

  def _recColored(self, retlist: List[ColoredPath], cycle: List[str],
                  retpath: ColoredPath=None, i=0,
                  prevcolor: COLOR=None) -> None:
    """
    Recrusive helper for `self._getColored()`.

    Parameters
    ----------
    retlist : List[ColoredPath]
        the return value
    cycle : List[str]
        the uncolored cycle to analyze
    retpath : ColoredPath, optional
        cycle build so far in the recursion, by default ColoredPath()
    i : int, optional
        the current vertex, by default 0
    prevcolor : COLOR, optional
        the color of the previous edge we looke at, by default ''
    """
    if not retpath:
      retpath = ColoredPath()

    u,v = cycle[i], cycle[(i+1) % len(cycle)]
    for key in self.G[u][v]:
      retpath.append(u, v, key)
      newcolor = self.G[u][v][key]['color']
      if i == 0:                              #Record first edge color to use
        retpath.setFirstColor(newcolor)       #when we get to the end of cycle

      if prevcolor == newcolor:
        retpath.addMonoVertex(u, newcolor)

      if len(retpath) == len(cycle):
        if retpath.firstcolor == newcolor:    #First vertex in cycle is
          retpath.addMonoVertex(v, newcolor)  #monochromatic.

        retlist.append(ColoredPath(retpath))

        if retpath.firstcolor == newcolor:
          retpath.removeMonoVertex(v)
      else:
        self._recColored(retlist, cycle, retpath, i+1, newcolor)

      retpath.pop()
      if prevcolor == newcolor:
        retpath.removeMonoVertex(u)


  def _getAlternatingEven(self, cycle: List[str]) -> List[PATH]:
    evencycles: List[PATH] = []
    self._recAlternatingEven(evencycles, cycle)
    return evencycles

  def _recAlternatingEven(self, allcycles: List[PATH], cycle: List[str],
                          i=0, color: COLOR=None, path: PATH=None) -> None:
    """
    Explore all alternating cycles that can be created from the cycle starting
    at position `i`. There may be 0 or many such cycles returned as a list in
    `allcycles`. This is necessary since a single cycle of vertices may have
    many different alternating cycles of edges.

    Warnings
    --------
    Only an even length cycle should be given to this function.

    Parameters
    ----------
    allcycles : List[PATH]
        the return list of PATHs
    cycle : List[str]
        the cycle (of nodes) we are building alternating cycles from
    i : int
        the current vertex in the cycle that we're looking at
    color : COLOR
        the color of the last vertex visited
    path : PATH
        the alternating path of length i that has been built so far
    """
    if not path:
      path = []

    u, v = cycle[i], cycle[(i+1) % len(cycle)]
    for k in self.G[u][v]:
      newcolor = self.G[u][v][k]['color']
      if color != newcolor:        #the path is alternating
        path.append((u, v, k))

        if len(path) == len(cycle):
          allcycles.append(list(path))
        else:
          self._recAlternatingEven(allcycles, cycle, i+1, newcolor, path)

        path.pop()

  def assertDecomposition(self, decomposition: List[ColoredPath]):
    """
    Confirm that all of the edges of `self.G` are covered by the decomposition.

    Parameters
    ----------
    decomposition : List[ColoredPath]
        the partition of the graph into cycles.
    """
    print(f'decomposition: {decomposition}')
    for c in decomposition:
      for edge in c:
        assert 'seenedge' not in self.G.edges[edge]
        self.G.edges[edge]['seenedge'] = True

    for edge in self.G.edges:
      if 'seenedge' not in self.G.edges[edge]:
        print(f'missing edge: {edge}')

    assert all('seenedge' in self.G.edges[edge] for edge in self.G.edges)

  def getCycleCompatibility(self, cycles: List[ColoredPath]
    ) -> Dict[ColoredPath, Dict[str, Set[ColoredPath]]]:
    """
    Find for each cycle all the vertices that have high degree >2 and get the
    set of compatible cycles for each of these vertices.

    Any length-3 subpath (trio) of a cycle has a set of compatible partner trios
    determined by the neighborhood of a gene in the input genomes. For example,
    trio (1h, 2t, 3h) represents the assignment of an adjacency 1,2 (or -2,-1)
    in the X genome to an adjacency 3,2 (or -3,-2) in Y. This trio is compatible
    with trio (5h, 2h, 6t) iff the adjacencies 5,-2 (or 2,-5) in X and 2,6 (or
    -6,-2) in Y exist.

    NOTES
    -----
      Assume that genomes `self.g1` and `self.g2` have only single chromosomes!

    Parameters
    ----------
    cycles : List[ColoredPath]
        all the simple alternating cycles of the graph

    Returns
    -------
    Dict[ColoredPath, Dict[str, Set[ColoredPath]]]
        map cycle to vertex (gene extremity) to the set of cycles that are
        compatible with it
    """
    assert len(self.g1.g) == 1 and len(self.g2.g) == 1, 'single chromosome expected'

    chrm1 = self.g1.g[0]
    chrm2 = self.g2.g[0]
    gTOi1 = defaultdict(list)
    gTOi2 = defaultdict(list)
    for i, gene in enumerate(chrm1):
      gTOi1[signless(gene)].append(i)
    for i, gene in enumerate(chrm2):
      gTOi2[signless(gene)].append(i)

      #Categorize genes by the number of occurences in each genome:
    matched = {}  #Multiple genes with same number of copies in both
    more1 = {}    #More copies in genome1
    more2 = {}    #More copies in genome2
    for gene in gTOi1.keys() | gTOi2.keys():
      count1 = len(gTOi1[gene])
      count2 = len(gTOi2[gene])
      if count1 > count2:
        more1[gene] = count1 - count2
      elif count2 > count1:
        more2[gene] = count2 - count1
      elif count1 > 1:
        matched[gene] = count1

    print(f'more1: {more1}')
    print(f'more2: {more2}')
    print(f'matched: {matched}')

      #Find the trios that are compatible with this trio:
    trioTOtrios: Dict[TRIO, Set[TRIO]] = defaultdict(set)
    for gene in matched:
      for i, j in product(gTOi1[gene], gTOi2[gene]):
        _, e1x = extremities(chrm1[i-1])
        e1, e2 = extremities(chrm1[i])
        e2x, _ = extremities(chrm1[i+1])

        _, e1y = extremities(chrm2[j-1])
        e2y, _ = extremities(chrm2[j+1])
        if chrm1[i] != chrm2[j]:    #different signs
          e1y, e2y = e2y, e1y

        t1 = (e1x, e1, e1y)
        t2 = (e2x, e2, e2y)
        trioTOtrios[t1].add(t2)
        trioTOtrios[t2].add(t1)
      
    print('___ Compatible Trios:')
    for trio, ts in trioTOtrios.items():
      print(f'{trio}: {ts}')

    trioTOcycles = defaultdict(set)
    for cycle in cycles:
      for v in cycle:
        for trio in trioTOtrios:
          if cycle.containsTrio(*trio):
            trioTOcycles[trio].add(cycle)
    
    print('___ Trio to cycles:')
    for trio, containing in trioTOcycles.items():
      print(f'{trio}: {containing}')

    cycleTOvTOcompatible: Dict[ColoredPath, Dict[str, Set[ColoredPath]]] =\
                          defaultdict(lambda: defaultdict(set))
    for t1, trios in trioTOtrios.items():
      cycles1 = trioTOcycles[t1]
      for t2 in trios:
        for c1, c2 in product(cycles1, trioTOcycles[t2]):
          cycleTOvTOcompatible[c1][t1[1]].add(c2)
          cycleTOvTOcompatible[c2][t2[1]].add(c1)

    print(f'compatible cycle pairs:')
    for c, vTOcompatible in cycleTOvTOcompatible.items():
      for v, compatibles in vTOcompatible.items():
        print(f'{c}, {v}:\n\t{compatibles}')

    return cycleTOvTOcompatible

  #def _getCycleBasis(self) -> List[List[str]]:
  #  # "ALGORITHMIC APPROACHES TO CIRCUIT ENUMERATION PROBLEMS AND APPLICATIONS"
  #  # (p14)
  #  # http://dspace.mit.edu/bitstream/handle/1721.1/68106/FTL_R_1982_07.pdf
  #  # "An algorithm for finding a fundamental set of cycles of a graph"

  #  u = list(self.G)[0]
  #  tovisit = [u]
  #  path = []
  #  cycles = []
  #  while len(tovisit):
  #    u = tovisit.pop()
  #    path.append(u)
  #    for v in self.G[u]:
  #      if v['visited']:
  #        cycles.append()

  #      for key in self.G[u][v]:
  #        print(self.G[u][v][0])
  #        if v['visited']:
  #          print(type(v))
  #        else:
  #          tovisit.append(v)
  #          v['visited'] = True

  #  return cycles

  def printGraph(self, filename: str, includesmall=True):
    """
    Print the graph in PDF to the file `filename`.

    Parameters
    ----------
    filename : str
        the pdf file to write to
    includesmall : bool, optional
        include the length-2 cycles, by default False
    """
    if includesmall:
      newG = copy.deepcopy(self.G)
    else:
      nodes = []
      for cc in nx.connected_components(self.G):
        if len(cc) > 2:
          nodes += cc

      newG = copy.deepcopy(self.G.subgraph(nodes))

    for u, v, k, color in newG.edges(data='color', keys=True):
      newG[u][v][k]['color'] = colorStr(color)

    printGraph(newG, filename)

  #def showGraph(self):
  #  #pos = nx.spring_layout(self.G)
  #  pos = nx.nx_agraph.graphviz_layout(self.G)
  #  #nx.draw(self.G, pos)
  #  #node_labels = nx.get_node_attributes(self.G, 'visited')
  #  #nx.draw_networkx_labels(self.G, pos, labels = node_labels)
  #  nx.draw(self.G, pos, node_size=500, with_labels=True,
  #          connectionstyle='arc3, rad = 0.1')

  #  edge_labels = nx.get_edge_attributes(self.G, 'color')
  #  print(edge_labels)
  #  #nx.draw_networkx_edge_labels(self.G, pos, edge_labels=edge_labels)
  #  #nx.draw(self.G, with_labels=True)

  #  plt.show()


#____________________________ Other Functions __________________________________

def getSimpleCycles(G: nx.Graph) -> List[List[str]]:
  """
  Return the list of all (exponentially many) simple cycles of the graph,
  irrespective of edge color. These are the simple cycles of the simple
  graph version of the multigraph `self.G` (multiple edges that link a single
  edge are considered one edge).

  Returns
  -------
  List[List[str]]
      cycles represented by vertex lists (each vertex appears once in a cycle)

  Examples
  --------
  >>> G = nx.Graph()
  >>> G.add_edges_from([(0,1), (0,2), (0,3), (1,3), (1,4), (1,5), (2,3),
                        (2,4), (2,5), (3,4), (4,5)])
  >>> print(sorted(getSimpleCycles(G)))
  [[0, 1, 4, 3, 2], [0, 1, 5, 2, 3], [0, 1, 5, 2, 4, 3], [0, 1, 5, 4, 2],
   [0, 1, 5, 4, 2, 3], [0, 2, 3], [0, 3, 4, 2], [1, 0, 2, 3],
   [1, 0, 2, 3, 4, 5], [1, 0, 2, 4], [1, 0, 2, 4, 3], [1, 0, 2, 5, 4, 3],
   [1, 0, 3], [1, 0, 3, 2, 5, 4], [1, 0, 3, 4], [1, 3, 0, 2, 4],
   [1, 3, 0, 2, 5, 4], [1, 3, 4], [1, 4, 2, 3], [1, 4, 2, 5], [1, 4, 5],
   [1, 4, 5, 2, 3], [1, 5, 2, 0, 3], [1, 5, 2, 0, 3, 4], [1, 5, 2, 3],
   [1, 5, 2, 3, 4], [1, 5, 2, 4, 3], [1, 5, 4, 2, 0, 3], [1, 5, 4, 2, 3],
   [1, 5, 4, 3], [2, 0, 1, 4, 5], [2, 0, 1, 5], [2, 0, 3, 4, 5], [2, 3, 4, 5],
   [2, 4, 1, 0, 3], [2, 5, 4], [3, 2, 4], [4, 3, 0, 1, 5]]
  >>> G = nx.Graph()
  >>> G.add_edges_from([(0,1), (0,2), (0,3), (1,3), (1,4), (1,5), (2,3),
                        (2,4), (2,5), (3,4), (4,5)])
  >>> print(sorted(getSimpleCycles(G)))
  [[1, 0, 2], [1, 0, 2, 3], [1, 0, 2, 4, 3], [1, 2, 3], [1, 2, 4, 3],
   [3, 2, 4]]
  """
  cycles = []
  for cc in nx.connected_components(G):
    C = G.subgraph(cc)
    basis = list(nx.cycle_basis(C))
    cycles += simpleCyclesFromBasis(basis)

  return cycles

def getCompleteTrees(G: nx.Graph, v: ColoredPath) -> List[FrozenSet[ColoredPath]]:
  """
  Given a compatability graph `G` where vertices represent colored cycles,
  find complete trees on `G`.  A complete tree is one where every
  monochromatic vertex in every cycle (vertex of `G`) is matched with exactly
  one monochromatic vertex from another cycle in the tree.

  Parameters
  ----------
  G : nx.Graph
      the compatability graph
  v : ColoredPath
      vertex of G, which is actualy an alternating cycle of the breakpoint graph

  Returns
  -------
  List[ColoredPath]
      the alternating cycle for each complete tree
  """
  completetrees = exploreSubtree(G, v)
  return completetrees

def exploreSubtree(G: nx.Graph, v: ColoredPath,
                   prev: ColoredPath=None) -> List[FrozenSet[ColoredPath]]:
  """
  Explore from vertex `v`, in a depth-first manner, until we come to the end
  of a path or a dead-end. Combine completed paths into sets that represent
  the complete subtrees that we have found.
  
  Parameters
  ----------
  G : nx.Graph
      the graph with BP graph cycles as vertices
  v : ColoredPath
      a vertex to explore from
  prev : ColoredPath
      the previous vertex that was visited

  Returns
  -------
  List[FrozenSet[ColoredPath]]
      a list of complete subtrees, each represented by a list of vertices

  Notes
  -----
      We build subtrees as sets of vertices. For the vertex `v` of `G` (which
      represents a cycle in the BP graph) we visit the subree of `G`
      departing from each monochromatic vertex (of BPG) in turn.
      If ever we run into a vertex of `G` that has been marked as visited then
      we know that there is a cycle in `G` and so this path cannot be added to
      the subtree.  If we come to the end of a path, then we have found a branch
      that can be passed back up the subtree. Each vertex `v` tests the
      combinations of the sets of paths coming from each of its monochromatic
      vertices. If any of those sets of paths is empty, then `v` cannot be the 
      root of a subtree coming from `prev`.
  """
  mvTOneigh = G.nodes[v]['mvTOneigh']
  if prev and len(v.mvTOcolor) == 1:        #arrived at leaf!
    return [frozenset([v])]

  if len(v.mvTOcolor) != len(mvTOneigh):    #not every monochromatic vertex has
    return []                               #a neighbor, so quit looking

  G.nodes[v]['visited'] = True
  allsubtreelists: List[List[FrozenSet[ColoredPath]]] = []
  for neighbors in mvTOneigh.values():
    if prev not in neighbors:
      completesubtrees = []
      for u in neighbors:
        if u != prev and not G.nodes[u]['visited']:
          completesubtrees += exploreSubtree(G, u, v)

      if completesubtrees:
        allsubtreelists.append(completesubtrees)
      else:                       #if there is no complete subtree for this mv
        G.nodes[v]['visited'] = False
        return []                 #then the whole tree cannot be complete

  G.nodes[v]['visited'] = False

    #We had at least one subtree attached to each monochromatic vertex
    # (that is not `prev`), so return all combinations of them.
  conflicts = defaultdict(set)    #pairs of subtrees that share a vertex
  for l1, l2 in combinations(allsubtreelists, 2):
    for subtree1, subtree2 in product(l1, l2):
      if not subtree1.isdisjoint(subtree2):
        conflicts[subtree1].add(subtree2)
        conflicts[subtree2].add(subtree1)

  return combineSubtrees(allsubtreelists, conflicts, [frozenset([v])])

def combineSubtrees(subtreelists: List[List[FrozenSet[ColoredPath]]],
                    conflicts: Dict[FrozenSet[ColoredPath], Set[FrozenSet[ColoredPath]]],
                    subtreessofar: List[FrozenSet[ColoredPath]],
                    i: int=0,
                    fullsubtrees: List[List[FrozenSet[ColoredPath]]]=None
                   ) -> List[FrozenSet[ColoredPath]]:
  """
  Recursively build combinations of subtrees from `subtreelists`. Go depth-first
  where at depth `i` we add to `subtreessfar` one of the subtrees
  (represented by sets of ColoredPaths) of `subtreelists[i]` and then continue
  to `i+1`.

  Parameters
  ----------
  subtreelists : List[List[FrozenSet[ColoredPath]]]
      a list of lists, one list for each monochromatic vertex, of subtrees
      (represented by sets of ColoredPaths)
  conflicts : Dict[FrozenSet[ColoredPath], Set[FrozenSet[ColoredPath]]]
      map a subtree to a set of subtrees that it conflicts with (shares a
      vertex with)
  subtreessofar : List[FrozenSet[ColoredPath]]
      combinations of subtrees built so far, represented as a list of subtrees
  i : int, optional
      index into `subtreelists` of current monochromatic vertex, by default 0
  fullsubtrees : List[List[FrozenSet[ColoredPath]]], optional
      list of fullsubtrees built so far, by default None

  Returns
  -------
  List[FrozenSet[ColoredPath]]
      [description]
  """
  if fullsubtrees is None:
    fullsubtrees = []

  if i == len(subtreelists):                 #Every list for every monochromatic
    fullsubtrees.append(list(subtreessofar)) #vertex has been visited.
    return []

  for subtree in subtreelists[i]:
    if disjointSubtrees(conflicts, subtree, subtreessofar):
      subtreessofar.append(subtree)
      combineSubtrees(subtreelists, conflicts, subtreessofar, i+1, fullsubtrees)
      subtreessofar.pop()

    #Each member of fullsubtrees is a list of subtree sets. Combine them into
    #a single set if we are at the top level of the recursion:
  if i == 0:
    return [frozenset.union(*treelist) for treelist in fullsubtrees]

  return []   #The return value is only used for the top of the recursion.

def disjointSubtrees(conflicts: Dict[FrozenSet[ColoredPath], Set[FrozenSet[ColoredPath]]],
                     subtree: FrozenSet[ColoredPath],
                     othersubtrees: List[FrozenSet[ColoredPath]]) -> bool:
  """
  Are all of the subtrees (sets) of one list independent of the all the
  subtrees of the other list?

  Parameters
  ----------
  conflicts : Dict[Set[ColoredPath], Set[Set[ColoredPath]]]
      map subtree to a set of subtrees that conflict with it
  subtree : Set[ColoredPath]
      list of subtrees
  othersubtrees : List[Set[ColoredPath]]
      other list of subtrees

  Returns
  -------
  bool
      True if none of the subtrees from one list conflict with the other
  """
  for othersubtree in othersubtrees:
    if othersubtree in conflicts[subtree]:
      return False

  return True




def simpleCyclesFromBasis(basis: List[List[str]]) -> List[List[str]]:
  """
  Given a cycle basis, return all simple cycles using the algorithm of Gibbs.

  Parameters
  ----------
  basis : List[List[str]]
      the cycle basis as lists of vertices

  Returns
  -------
  List[str]
      all simple cycles of the graph

  Notes
  -----
  The algorithm uses linear combinations of the cycles in the basis according
  to [1]_ (also described on p14 of [2]_).

  References
  ----------
  .. [1] Gibbs, N.E. A Cycle Generation Algorithm for Finite Undirected Linear
     Graphs. Journal of the ACM 16, 4 (Oct 1969), 564-568.
  .. [2] ALGORITHMIC APPROACHES TO CIRCUIT ENUMERATION PROBLEMS AND APPLICATIONS
     http://dspace.mit.edu/bitstream/handle/1721.1/68106/FTL_R_1982_07.pdf
  """
  if not basis:   #there are no cycles in the graph
    return []

  basisedges = []
  for scycle in basis:
    basisedges.append([sortedPair(u, v) for u, v in pairwise(scycle)] +
                      [sortedPair(scycle[-1], scycle[0])])

  allcycles = [frozenset(basisedges[0])]    #Step 1
  combos = [frozenset(basisedges[0])]
  i = 0
  for cycle in map(lambda x: frozenset(x), basisedges[1:]):
    i += 1
    tokeep = set()
    therest = set()
    for other in combos:                    #Step 2
      if cycle & other:
        tokeep.add(cycle ^ other)
      else:
        therest.add(cycle ^ other)

    redundant = set()
    for c1, c2 in combinations(tokeep, 2):  #Step 3
      if c1 < c2:
        redundant.add(c2)
      if c2 < c1:
        redundant.add(c1)
                                            #Step 4
    allcycles.extend(c for c in tokeep if c not in redundant)
    allcycles.append(cycle)
                                            #Step 5
    combos.extend(tokeep)
    combos.extend(therest)
    combos.append(cycle)

  retcycles = []
  for cycle in allcycles:
    retcycles.append(getOrderedVertices(cycle))

  return retcycles

def sortedPair(u: str, v: str) -> Tuple[str, str]:
  if u <= v:
    return (u, v)
  else:
    return (v, u)

def getOrderedVertices(cycleset: FrozenSet[Tuple[str, str]]) -> List[str]:
  """
  Convert each set of edges into a list of vertices that come in cycle order.

  Parameters
  ----------
  cyclesets : List[Set[Tuple[str, str]]]
      the cycles as sets of edges

  Returns
  -------
  List[str]
      the cycles as lists of ordered vertices
  """
  vTOneighbors = defaultdict(list)
  start = ''
  for edge in cycleset:
    vTOneighbors[edge[0]].append(edge[1])
    vTOneighbors[edge[1]].append(edge[0])
    start = edge[0]
  
  path = [start]
  v = vTOneighbors[start][0]
  while v != start:
    nextv = vTOneighbors[v][0]
    if nextv == path[-1]:
      nextv = vTOneighbors[v][1]

    path.append(v)
    v = nextv

  return path


def extremities(gene: str) -> Tuple[str, str]:
  """
  Return a pair of gene names, 'g_h' and 'g_t' for a gene 'g' in the order
  according to the sign of the gene.

  Parameters
  ----------
  gene : str
      the gene to get the extremities from

  Returns
  -------
  Tuple[str, str]
      the two extremities
  """
  first, second = TAIL, HEAD
  if str(gene)[0] == '-':
    first, second = second, first

  gene = unsigned(gene)
  return gene+first, gene+second

def trios(iterable, wrap=False):
  "s -> (s0,s1,s2), (s1,s2,s3), (s2,s3,s4), ..."
  a, b, c = tee(iterable, 3)
  next(b, None)
  first = next(c, None)
  second = next(c, None)
  for trio in zip(a, b, c):
    lastone = trio
    yield trio

  if wrap:
    lastone = trio[1]
    lasttwo = trio[2]
    yield (lastone, lasttwo, first)
    yield (lasttwo, first, second)

def pairwise(iterable, wrap=False):
  "s -> (s0,s1), (s1,s2), (s2, s3), ..."
  a, b = tee(iterable)
  first = next(b, None)
  if wrap:
    return zip_longest(a, b, fillvalue=first)

  return zip(a, b)

def choose2(n: int) -> int:
  return int((n * (n-1)) / 2)

def printGraph(G: nx.Graph, filename: str):
  nx.nx_agraph.to_agraph(G).draw(filename, prog='dot')

def colorStr(color: COLOR) -> str:
  if color == GRAY:
    return 'gray'

  return 'black'

def otherColor(color: COLOR) -> COLOR:
  if color == GRAY:
    return BLACK

  return GRAY

def signless(gene: str) -> str:
  if gene[0] == '-':
    return gene[1:]

  return gene
