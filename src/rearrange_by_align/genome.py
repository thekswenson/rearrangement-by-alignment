import itertools
import sys

from typing import Any, Iterable, Iterator
from typing import List
from typing import Tuple
from typing import TextIO

from networkx.utils.misc import iterable
from . import CHROMMATCHES, CIRCULAR, INTMATCH, LINEAR
import re

from .suffixarray import SuffixArray
from .listops import rotateList
from .listops import fixInverted
from .listops import removeSigns
from .listops import getInverted
from .listops import MissingOriginError

class Chromosome():
  """
  A Chromosome that acts like a list. It knows how to get matching substrings
  with other Chromosomes using its suffix array.

  Attributes
  ----------
  c : List[str]
    The genome as a list of strings (each string a gene)
  num : int
    The chromosome number
  linear : bool
    Is the chromosome linear? (and not circular)
  sa : SuffixArray
    The suffix array for finding common substrings with other genomes.
  """
  def __init__(self, l: List[str], num: int, linear=False, rotate=False,
               invertneg=False, removesigns=False, buildsa=True):
    """
    Prepare the Chromosome for comparison. Do things like rotate or invert
    negative segments, depending on the flags.

    Parameters
    ----------
    l : List[str]
        the genome as a list
    num : int
        the chromosome number
    linear : bool, optional
        is the chromosome linear?, by default False
    rotate : bool, optional
        rotate the chromosome to start with ORIGIN, by default True
    invertneg : bool, optional
        invert the negative parts of the chromosome, by default False
    removesigns : bool, optional
        make all negative genes positive, by default False
    buildsa : bool, optional
        build the suffix array
    """
    self.c = l
    self.num = num
    self.linear = linear
    self._stale = True
    self._prepareChromosome(rotate, invertneg, removesigns)

    if buildsa:
      self.sa = SuffixArray(self.c)
      self._stale = False


  def getCommonSubstrings(self, other: 'Chromosome', getall=True,
                          ignoreoverlap=False, invertother=False, minlen=1
                          ) -> List[INTMATCH]:
    """
    Get all common substrings between this Chromosome and `other`.
    If `getall == False` then only return the maximal common substrings
    (can't be extended).

    Parameters
    ----------
    other : Chromosome
        other genome to compare to
    getall : bool
        get all of the common substrings instead of just the maximal
    ignoreoverlap : bool
        ignore common substrings that have overlaping indices (useful when
        matching a string against itself)
    invertother : bool
        compare `self` to the inverted version of `other`
    minlen : int
        report matches of at least this length

    Returns
    -------
    List[MATCH]
        list of all common substrings as intervals pairs
    """
    assert not self._stale
    return self.sa.getCommonSubstrings(other.c, getall, ignoreoverlap,
                                       invertother, minlen)

  def inverted(self) -> 'Chromosome':
    """
    Return the inverted version of this Genome.
    """
    return Chromosome(getInverted(self.c), self.num, self.linear, buildsa=False)

  def _prepareChromosome(self, rotate=False, invertneg=False,
                         removesigns=False):
    """
    Prepare the Chromosome for comparison. Do things like rotate or invert
    negative segments, depending on the flags.

    warn: the suffix array will be out of date after calling this

    Parameters
    ----------
    rotate : bool, optional
        rotate the chromosome to start with ORIGIN, by default True
    invertneg : bool, optional
        invert the negative parts of the chromosome, by default False
    removesigns : bool, optional
        make all negative genes positive, by default False

    Returns
    -------
    List[str]
        the new chromosome
    """
    if rotate:
      try:
        self.c = rotateList(self.c)
      except MissingOriginError as e:
        sys.exit(f'Error: {e}')

    if invertneg:
      fixInverted(self.c)

    if(removesigns):
      removeSigns(self.c)

  def __getitem__(self, key):
    if isinstance(key, int):
      return self.c[key]
    else:         #the key is a slice
      return Chromosome(self.c[key], self.num, self.linear, buildsa=False)

  def __add__(self, other: 'Chromosome') -> 'Chromosome':
    assert self.linear == other.linear, 'Adding linear chromosome to circular.'
    return Chromosome(self.c+other.c, self.num, self.linear, buildsa=False)

  def __len__(self) -> int:
    return len(self.c)

  def __str__(self) -> str:
    marker = LINEAR if self.linear else CIRCULAR
    return ', '.join(self.c) + f' {marker}'

  def __iter__(self) -> Iterator[str]:
    return iter(self.c)


class Genome():
  """
  A genome has a list of Chromosomes.

  Attributes
  ----------
  g : List[Chromosome]
    The genome as a list of Chromosomes
  """
  def __init__(self, l: List[Chromosome]):
    """
    Parameters
    ----------
    l : List[Chromosome]
        the genome as a list
    """
    self.g = l

  def isSingleChromosome(self):
    return len(self.g) == 1

  def getCommonSubstrings(self, other: 'Genome', getall=True,
                          ignoreoverlap=False, invertother=False, minlen=1
                          ) -> List[CHROMMATCHES]:
    """
    Get all common substrings between this genome and `other`.
    If `getall == False` then only return the maximal common substrings
    (can't be extended).

    Parameters
    ----------
    other : Genome
        other genome to compare to
    getall : bool
        get all of the common substrings instead of just the maximal
    ignoreoverlap : bool
        ignore common substrings that have overlaping indices (useful when
        matching a string against itself)
    invertother : bool
        compare `self` to the inverted version of `other`
    minlen : int
        report matches of at least this length

    Returns
    -------
    List[CHROMMATCHES]
        list of all common substrings as intervals pairs
    """
    matches = []
    for c1, c2 in itertools.product(self.g, other.g):
      common = c1.getCommonSubstrings(c2, getall, ignoreoverlap, invertother,
                                       minlen)
      matches.append((c1.num, c2.num, common))

    return matches

  def __len__(self) -> int:
    return sum(map(len, self.g))

  def __str__(self) -> str:
    return '\n'.join(map(str, self.g))

  def __iter__(self) -> Iterator[Chromosome]:
    return iter(self.g)


#_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
#-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-


commentre = re.compile(r'\s*(##|//)')
fastare = re.compile(r'\s*>')
def getGenomes(filename: str, rotate=False, invertneg=False, removesigns=False,
               number=0) -> List[Tuple[str, Genome]]:
  """
  Return the genomes from the given lines in the form of description, genome
  pairs. The genome format is the UniMoG format described here:

    https://bibiserv.cebitec.uni-bielefeld.de/dcj?id=dcj_manual

  with two important exceptions:
    1. the lines without a chromosome specifier ('|' or ')') default to circular
       instead of linear.
    2. the existence of the character ';' or ',' in the genome implies that
       that character is to be used as the delimiter between genes, rather than
       whitespace.

  Parameters
  ----------
  filename : str
      the input filename
  rotate : bool, optional
      rotate the genome to start with the ORIGIN, by default True
  number : int, optional
      stop reading file after this many genomes, by default None
  invertneg : bool, optional
      invert the negative parts of the genome, by default False
  removesigns : bool, optional
      make all negative genes positive, by default False

  Returns
  -------
  List[Tuple[str, Genome]]
      list of description, Genome tuples
  """
  with open(filename) as f:
    retval = []
    line = getNonWhitespaceLine(f)
    numread = 0

    while line and commentre.match(line):   #remove leading comments
      line = getNonWhitespaceLine(f)

    while line:
      if not fastare.match(line):
        sys.exit(f'Error: missing description line in input file.')

      description = line.rstrip()
      line = getNonWhitespaceLine(f)
      if not line:
        sys.exit(f'Error: description line without following genome.')
      
      genomelines = line
      while line and not fastare.match(line):
        if not commentre.match(line):
          genomelines += line
        line = getNonWhitespaceLine(f)

      genome = parseGenome(genomelines, rotate, invertneg)
      retval.append((description, genome))

      numread += 1
      if number and numread == number:
        break

      line = getNonWhitespaceLine(f)

  return retval

def parseGenome(genome: str, rotate=False, invertneg=False, removesigns=False
                ) -> Genome:
  """
  Parse the genomes, where genes are delimited by ';' or ',', or by whitespace.
  Chromosomes end in either ')' or '|' to denote either circular or linear
  chromosomes. When lacking a chromosome character, the chromosome is assumed
  to be circular.
  Return the genome. Process the genomes further depending on the given
  flags.
  """
  genome = genome.replace('\n', ' ').strip()  # genome can span multiple lines
  if genome[-1] != CIRCULAR and genome[-1] != LINEAR:
    genome += f' {CIRCULAR}'                  # default to circular

  chromspecre = rf'({re.escape(CIRCULAR)}|{re.escape(LINEAR)})'
  chromstrings = list(map(str.strip, re.split(chromspecre, genome)))

  chromosomes = []
  counter = 0
  for cs, chromchar in twoAtATime(chromstrings):
    if not cs:
      continue

    if ',' in cs or ';' in cs:
      delim_re = re.compile(r";|,")
      glist = [gene.strip() for gene in delim_re.split(cs.strip())]
    else:
      glist = cs.split()

    if any(g == '' for g in glist):
      raise(Exception('empty gene name found (is there a trailing comma?)'))

    islinear = chromchar == LINEAR
    chromosomes.append(Chromosome(glist, counter, islinear, rotate, invertneg,
                                  removesigns))
    counter += 1

  return Genome(chromosomes)


def getGenome(filename: str, rotate=False, invertneg=False, removesigns=False,
              ) -> Tuple[str, Genome]:
  """
  Read the first genome in the file and return a description/genome pair.

  Parameters
  ----------
  filename : str
      the file with the genome
  rotate : bool, optional
      rotate the genome to start with the ORIGIN, by default True
  invertneg : bool, optional
      invert the negative parts of the genome, by default False
  removesigns : bool, optional
      make all negative genes positive, by default False

  Returns
  -------
  Tuple[str, Genome]
      description/genome pair
  """
  genomes = getGenomes(filename, rotate, invertneg, removesigns, 1)
  if genomes:
    return genomes[0]
  else:
    raise(Exception('Genome missing in input file.'))

def getNonWhitespaceLine(f: TextIO) -> str:
  """
  Return the first line that is not just whitespace.

  Parameters
  ----------
  f : TextIO
      The open file object
  """
  line = f.readline()
  while line and line.isspace():
    line = f.readline()

  return line


def unsigned(g: str) -> str:
  """
  Return the unsigned version of the gene if it is negated.

  note: could use MemoryView byte encodings to improve speed.

  Parameters
  ----------
  g : str
      the gene

  Returns
  -------
  str
      the gene without the '-' at the start
  """
  if str(g)[0] == '-':
    return g[1:]

  return g

def twoAtATime(iterable: Iterable[Any]) -> Iterator[Tuple[Any, Any]]:
    """
    s -> (s0, s1), (s2, s3), (s4, s5), ...
    """
    a = iter(iterable)
    return zip(a, a)
