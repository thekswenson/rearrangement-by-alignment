
SLACK_TOLERANCE = 1.0e-5  #The amount of slack we will allow in the solution.

def getLPstr(p, removespecialchars=False):
  """
  Print the ILP.
  """
  variables = p.variables.get_names()
  coefficients = map(str, p.objective.get_linear())
  terms = map("*".join,zip(coefficients, variables))

  retstring = "Linear Programming Objective Function & Constraints\n"+\
              "***************************************************\n"+\
              "minimize: "
  retstring += " + ".join(terms) + "\n\ns.t.:\n"

  senses = p.linear_constraints.get_senses()
  rhs = p.linear_constraints.get_rhs()
  i=0
  for row in p.linear_constraints.get_rows():
    _, coefficients = row.unpack()
    first = True
    for coefficient, var in zip(coefficients, p.variables.get_names(row.ind)):
      if coefficient > 0 and not first:
        retstring += ' +'

      if coefficient != 1:
        retstring += f' {coefficient}*{var}'
      else:
        retstring += f' {var}'

      first = False

    retstring += " "+translate(senses[i])+" "+str(rhs[i])+"\n"
    i += 1

  if removespecialchars:
    retstring = retstring.replace('[','')
    retstring = retstring.replace(']','')
    retstring = retstring.replace('-','')

  return retstring


def translate(sense):
  """
  Translate the CPLEX sense id into a readable format.
  """
  if(sense == 'E'):
    return '='
  if(sense == 'L'):
    return '<='
  if(sense == 'G'):
    return '>='

  return sense


def lowSlack(p):
  """
  Given the CPLEX problem, return False if any of the slack values are
  above tolerance (i.e. farther from 0 or 1 than SLACK_TOLERANCE).
  """
  slack = p.solution.get_linear_slacks()
  for j in range(p.linear_constraints.get_num()):
    if(slack[j] < (1-SLACK_TOLERANCE) and slack[j] > SLACK_TOLERANCE):
      return False
  return True

