from copy import deepcopy
from typing import Tuple
from typing import List
from . import INTERVAL
from collections import deque

from . import ORIGIN

def getInvertedList(l: List[Tuple[List[str], INTERVAL]]
                    ) -> List[Tuple[List[str], INTERVAL]]:
  """
  Invert each of the signed lists (signed) in the given list.
  """
  retval = deepcopy(l)
  for substring, _ in retval:
    invertAll(substring)

  return retval


def getInverted(l: List[str]) -> List[str]:
  """
  Return an inverted version of the signed list.
  """
  retstring = deepcopy(l)
  invertAll(retstring)

  return retstring


def invertAll(l: List[str], signed=True):
  """
  Invert the given list.
  """
  invert(l, 0,len(l)-1, signed)

  

def invert(s, i,j, signed=True):
  """
  Apply the given inversion to the given sequence.
  """
  s[i:(j+1)] = reversed(s[i:(j+1)])

  if(signed):
    invertSigns(s, i,j)


def invertSigns(s, i,j):
  """
  Switch the signs of the ith through jth elements.
  """
  for x in range(i, j+1):
    if(s[x][0] == "-"):
      s[x] = s[x][1:]
    else:
      s[x] = "-"+s[x]


def removeSigns(g):
   """
   Remove negative signs from elements
   """
   s = g[1]
   l = len(s)

   i = 0
   while i < l:
     if(s[i][0] == "-"):
       s[i] = s[i][1:]
     i += 1



def fixInverted(s: List):
   """
   Fix the inverted subsequences in the given list.
   (i.e. do an inversion on each contiguous string of negative elements)
   """
   length = len(s)

   i = 0
   while i < length:
     while i < length and s[i][0] != "-":
       i += 1 
     if(i == length):
       continue
      
     j = i+1
     while j < length and s[j][0] == "-":
       j += 1

     invert(s, i,j-1)
     i = j


def rotateGenome(genome):
  """
  Rotate the given genome so that it starts with the origin.
  """
  g = genome.replace(";",",").split(",")
  g =  deque(map(str.strip, g))
  i = 0
  while i < len(g) and g[i] != ORIGIN:
    i += 1 

  if(g[i] == ORIGIN): 
    g.rotate(-i)
  
  return list(g)

def rotateList(g: List[str]):
  """
  Rotate the given genome so that it starts with the origin.
  """
  i = 0
  while i < len(g) and g[i] != ORIGIN:
    i += 1 

  if i >= len(g):
    raise(MissingOriginError(f'missing origin character "{ORIGIN}" to rotate to'))

  if g[i] == ORIGIN: 
    g = g[i:] + g[:i]

  return g
  

def toStr(l):
  """
  Convert the given list to a space delimited string.
  """
  retval = ""
  for c in l:
    retval += c+","
  return retval[:-1]




def overlaps(i1: INTERVAL, i2: INTERVAL) -> bool:
  """
  Return True if the two intervals overlap.
  """
  return ((i1[0] >= i2[0] and i1[0] < i2[1]) or  #If either of the endpoints
          (i1[1] > i2[0] and i1[1] <= i2[1]) or  #of i1 are within i2 or if an
          (i2[0] >= i1[0] and i2[0] < i1[1]))    #endpoint of i2 is within i1.



#E|E|E|E|E|E|E|E|E|E|E|E|E|E|E|E|E|E|E|E|E|E|E|E|E|E|E|E|E|E|E|E|E|E|E|E|E|E|E|E

class MissingOriginError(Exception): 
  def __init__(self, value): 
    super().__init__() 
    self.value = value 
 
  def __str__(self): 
    return self.value 
