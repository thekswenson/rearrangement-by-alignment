"""
This implements a simple suffix array.

It is based on the excellent post at:
https://louisabraham.github.io/notebooks/suffix_arrays.html
"""
from itertools import zip_longest
from itertools import islice
from typing import Iterable
from typing import List
from typing import Tuple
from . import INTERVAL
from . import INTMATCH

from .listops import getInverted
from .listops import overlaps

class SuffixArray:
  """
  A suffix array datastructure for getting common substrings.

  Attributes
  ----------
  s : Iterable
    The string that this suffix array indexes.
  array : List
    The suffix array. `self.array[i]` is the index in `self.s` of the ith
    largest suffix when the suffixes are lexicographically ordered.
  """
  def __init__(self, l: List):
    """
    Construct a suffix array from the give iterable.
    """
    self.s = l
    self.array = list(self._suffix_array())

  def getCommonSubstrings(self, other: List[str], getall=True,
                          ignoreoverlap=False, invertother=False, minlen=1
                          ) -> List[INTMATCH]:
    """
    Get all common substrings between `self.s` and `other`. If `getall == False`
    then only return the maximal common substrings (can't be extended).

    Parameters
    ----------
    other : Iterable
        other string
    getall : bool
        get all of the common substrings instead of just the maximal
    ignoreoverlap : bool
        ignore common substrings that have overlap indices
    invertother : bool
        compare `self.s` to the inverted version of `other`
    minlen : int
        report matches of at least this length

    Returns
    -------
    List[INTMATCH]
        list of all common substrings
    """
    if invertother:
      other = getInverted(other)

    commonsubstrings = []
    for i in range(len(other)):
      commonsubstrings += self.matchPrefix(other, i, getall, ignoreoverlap,
                                           minlen)

    if invertother:                 #Fix the inverted indices:
      corrected = []
      for s1, (begin, end) in commonsubstrings:
        corrected.append((s1, (len(other)-end, len(other)-begin)))

      return corrected

    return commonsubstrings

  def matchPrefix(self, other: List, index: int, getall=True,
                  ignoreoverlap = False, minlen = 1
                  ) -> List[INTMATCH]:
    """
    Get all suffixes that match a prefix of `other`. These are all common
    substrings between `self.s` and a prefix of `other`.

    Parameters
    ----------
    other : Iterable
        other string
    index : Iterable
        prefex starting at `other[index]`
    getall : bool
        get all of the common substrings instead of just the maximal
    ignoreoverlap : bool
        ignore common substrings that have overlap indices
    minlen : int
        the minimum length interval to be kept

    Returns
    -------
    List[INTMATCH]
        list of pairs of intervals ((self_s, self_e), (origin_s, origin_e))
    """
    assert(index < len(other))

      #Find the SA indices that point to matching prefixes:
    start, end = self._findSAInterval(other, index)

      #Collect the indices for all matches:
    retlist = []
    for i in range(start, end):
      length = 0
      sindex = self.array[i]
      while length < min(len(other) - index, len(self) - sindex):
        if other[index + length] != self.s[sindex + length]:
          break
        length += 1

      assert length
      retlist += self._buildIntervalPairs(sindex, index, length,
                                          getall, ignoreoverlap, minlen)

      #TODO: remove temp sanity check
    for (sstart, send), (others, othere) in retlist:
      assert other[others:othere] == self.s[sstart:send]

    return retlist

  def _findSAInterval(self, other: List[str], index: int) -> INTERVAL:
    """
    Return the interval of indices indices of `self.array` that point to
    prefixes of `self.s` sharing a begining with `other[index:]`.
    
    note: python indexed, so if (start, end) is returned, then
          `self.s[self.array[end]]` will NOT share a prefix, but
          `self.s[self.array[end-1]]` will share a prefix.

    Parameters
    ----------
    other : List[str]
        search for mathing prefixes to this string
    index : int
        starting at this index

    Returns
    -------
    INTERVAL
        interval (python indexed) pointing to matching prefixes
    """
      #Do a binary search to find the smallest index in self.array that points
      #to a matching suffix:
    left, right = 0, len(self)
    while left < right:
      mid = (left + right)//2
      if other[index] > self.s[self.array[mid]]:
        left = mid + 1  #other is after the midpoint
      else:
        right = mid     #other is before (or equal to) the midpoint
 
    start = left
    
      #Do a binary search to find the smallest index in self.array that
      #is after start, and points to a non-matching suffix:
    right = len(self)
    while left < right:
      mid = (left + right)//2
      if self.s[self.array[mid]] == other[index]:
        left = mid + 1
      else:
        right = mid


    if start < right:
      assert all(other[index] == self.s[self.array[i]] for i in range(start, right))
      assert all(other[index] != self.s[self.array[i]] for i in range(start))
      assert all(other[index] != self.s[self.array[i]] for i in range(right, len(self)))

    return start, right

  def _buildIntervalPairs(self, start1: int, start2: int, length: int,
                          getall=True, ignoreoverlap=False, minlen=1
                          ) -> List[INTMATCH]:
    """
    Get a pairs of intervals that start with the given start indices and
    end `length-1` to the right of the start. If getall is true, then get all
    pairs of subintervals that start at the same location.

    Parameters
    ----------
    start1 : int
        start index of first interval
    start2 : int
        start index of second interval
    length : int
        the factor to add to the start
    getall : bool
        get all of the common substrings instead of just the maximal
    ignoreoverlap : bool
        ignore common substrings that have overlap indices
    minlen : int
        the minimum length interval to be kept

    Returns
    -------
    List[INTMATCH]
        list of pairs of intervals
    """
    if ignoreoverlap and start1 == start2:
      return []

    retlist = []
    if length >= minlen:
      if not getall:
        minlen = length   #Only add the largest interval.

      for tlength in range(minlen, length+1):
        ipair = ((start1, start1+tlength), (start2, start2+tlength))
        if not ignoreoverlap or not overlaps(*ipair):
          retlist.append(((start1, start1+tlength), (start2, start2+tlength)))

    return retlist



  def __len__(self):
    return len(self.array)

  def _suffix_array(self):
    """
    suffix array of s
    O(n * log(n)^2)
    """
    n = len(self.s)
    k = 1
    line = self._to_int_keys(self.s)
    while max(line) < n - 1:
      line = self._to_int_keys(
        [a * (n + 1) + b + 1
         for (a, b) in
         zip_longest(line, islice(line, k, None),
                     fillvalue=-1)])
      k <<= 1
    return self._inverse_array(line)

  def _to_int_keys(self, l: Iterable):
    """
    l: iterable of keys
    returns: a list with integer keys
    """
    seen = set()
    ls = []
    for e in l:
        if not e in seen:
            ls.append(e)
            seen.add(e)
    ls.sort()
    index = {v: i for i, v in enumerate(ls)}
    return [index[v] for v in l]

  def _inverse_array(self, l):
    n = len(l)
    ans = [0] * n
    for i in range(n):
      ans[l[i]] = i
 
    return ans
